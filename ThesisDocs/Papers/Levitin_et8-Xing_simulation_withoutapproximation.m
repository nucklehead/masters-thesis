clear;
clc;
tic;
eta=[100,60,160,90,120,90,87];
beta=[1.2,1.0,1.0,1.1,1.0,1.0,1.0];
v=[40,25,20,30,43,28,37];
w=[5,2,6,4,5,5,5];
lambda=[10,3,7,11,8,8,8];
epsilon=[0.90,0.93,0.70,0.85,0.95,0.99,0.91];
tau=150;%duration of the mission
sigma=15;%backup time
mypi=0.22;%fraction of backup
H=4;%times of backup
alpha=mypi*tau+sigma;%duration of one cycle
myT=tau+H*sigma;%mission time without failures

M_sample=1000;
flag=zeros(1,M_sample);
cost=zeros(1,M_sample);
missionT=zeros(1,M_sample);
ite=0;
while (ite<M_sample)
    M_duration=0;% recording the mission time
    current_T=0;% recoding the schedule of the mission
    current_com=1;% index of the current working component
    total_cost=0;% total cost
    while (current_T<myT&&current_com<=1)
        current_T=floor(current_T/alpha)*alpha;
        if (current_com>1)
            M_duration=M_duration+lambda(current_com);
            total_cost=total_cost+v(current_com);
            if (rand(1)>(epsilon(current_com)))%failed replacement
                current_com=current_com+1;%单元数要加1
                continue;
            end
        end
        tmpT=(-log(rand(1)))^(1/beta(current_com))*eta(current_com);%working time of current component
        total_cost=total_cost+w(current_com)*(min([myT-current_T,tmpT]));
        M_duration=M_duration+(min([myT-current_T,tmpT]));
        current_com=current_com+1;
        current_T=current_T+tmpT;
    end
    ite=ite+1;
    if current_T>=myT
        flag(ite)=1;
    end
    cost(ite)=total_cost;
    missionT(ite)=M_duration;
end
sum(flag)/M_sample
mean(cost)
mean(missionT)
toc;