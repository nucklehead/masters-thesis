import com.mongodb.DBObject
import com.mongodb.MongoClient
import com.mongodb.util.JSON

/**
 * Created by pierrj1 on 6/9/2015.
 */


def  mongoClient = null;
try {
    mongoClient = new MongoClient( "localhost" );
} catch (UnknownHostException e) {
    e.printStackTrace();
    System.exit(-1);
}
db = mongoClient.getDB( "mydb" );

def origElements = db.getCollection("elements");

def numElements = origElements.count

def nameA = "A"
def nameB= "B"
def prefixA = "Series_"
def prefixB = "Parallel_"
def numA = 20
def numB = 20

def query = (DBObject) JSON.parse("{}");
query.put("name", nameA)
def elementA = origElements.find(query).next()
query.put("name", nameB)
def elementB = origElements.find(query).next()

(1..numA).each {index ->
    elementA.put("name", prefixA + nameA)
    elementA.put("id", "el-" + (numElements + index))
    elementA.removeField("_id")
    origElements.insert(elementA);
}

(1..numB).each {index ->
    elementB.put("name", prefixB + nameB)
    elementB.put("id", "el-" + (numElements + index + numA))
    elementB.removeField("_id")
    origElements.insert(elementB);
}