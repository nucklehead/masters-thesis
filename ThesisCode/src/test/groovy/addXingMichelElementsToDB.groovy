import com.mongodb.DBCursor
import com.mongodb.DBObject
import com.mongodb.MongoClient
import com.mongodb.util.JSON
import distibutionFunctions.ExpFailFct
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.map.ObjectWriter
import simulation.Element

/**
 * Created by pierrj1 on 6/9/2015.
 */


def  mongoClient = null;
try {
    mongoClient = new MongoClient( "localhost" );
} catch (UnknownHostException e) {
    e.printStackTrace();
    System.exit(-1);
}
db = mongoClient.getDB( "mydb" );

def origElements = db.getCollection("elements");
//DB has all this with id =null :-(
def infos = [
        [name: "HECS_A", lambda: 0.0001, dh: 1.0, dw: 1.0],
        [name: "HECS_A1", lambda: 0.0001, dh: 1.0, dw: 1.0],
        [name: "HECS_A2", lambda: 0.0001, dh: 1.0, dw: 1.0],
        [name: "HECS_M1", lambda: 0.00006, dh: 1.0, dw: 1.0],
        [name: "HECS_M2", lambda: 0.00006, dh: 1.0, dw: 1.0],
        [name: "HECS_M3", lambda: 0.00006, dh: 1.0, dw: 1.0],
        [name: "HECS_M4", lambda: 0.00006, dh: 1.0, dw: 1.0],
        [name: "HECS_M5", lambda: 0.00006, dh: 1.0, dw: 1.0],
        [name: "HECS_MIU1", lambda: 0.00005, dh: 1.0, dw: 1.0],
        [name: "HECS_MIU2", lambda: 0.00005, dh: 1.0, dw: 1.0],
        [name: "HECS_BUS1", lambda: 0.000001, dh: 1.0, dw: 1.0],
        [name: "HECS_BUS2", lambda: 0.000001, dh: 1.0, dw: 1.0],
        [name: "HECS_HW", lambda: 0.00005, dh: 1.0, dw: 1.0],
        [name: "HECS_SW", lambda: 0.03, dh: 1.0, dw: 1.0],
        [name: "HECS_OP", lambda: 0.001, dh: 1.0, dw: 1.0],
]

def ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
def size = origElements.count

infos.each {info ->
    def element = new Element()
    element.name = info.name
    element.failiureFunct = new ExpFailFct(info.lambda);
    element.dh = info.dh
    element.dw = info.dw
    element.id = "el-" + (infos.indexOf(info) + size + 1)

    def json = ow.writeValueAsString(element);
    def dbObject = (DBObject) JSON.parse(json);

    def dbObject2 = (DBObject) JSON.parse("{dw:}");
    dbObject2.put("name", info.name)
    origElements.update(dbObject2, dbObject)
//
//    origElements.remove(origElements.find(dbObject2).next())

//    origElements.insert(dbObject);

}