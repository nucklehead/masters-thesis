import com.mongodb.DBCursor
import com.mongodb.MongoClient

/**
 * Created by pierrj1 on 6/9/2015.
 */


def  mongoClient = null;
try {
    mongoClient = new MongoClient( "localhost" );
} catch (UnknownHostException e) {
    e.printStackTrace();
    System.exit(-1);
}
db = mongoClient.getDB( "mydb" );

def origElements = db.getCollection("elements");
DBCursor cursor = origElements.find();
while(cursor.hasNext()) {
    def obj = cursor.next()
    println obj.get("id") + ":::" + obj.get("state")
    println()
}