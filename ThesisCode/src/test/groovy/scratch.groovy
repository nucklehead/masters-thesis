import com.mongodb.DBObject
import com.mongodb.util.JSON
import distibutionFunctions.ConstFct
import distibutionFunctions.ExpFailFct
import distibutionFunctions.UserDefineFct
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.map.ObjectWriter
import org.matheclipse.core.eval.EvalUtilities
import org.matheclipse.core.interfaces.ISignedNumber
import simulation.Element

/**
 * Created by pierrj1 on 2/25/2015.
 */
ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
String json = ow.writeValueAsString(new Element());
DBObject dbObject = (DBObject) JSON.parse(json);
println dbObject


def ast = new UserDefineFct("1-(-1+Exp(-10t)+3)")
println ast.toListRep()
println ast.timeDistribution()


ast = new UserDefineFct("1+3*2t")
println ast.toListRep()
println ast.timeDistribution()

ast = new UserDefineFct(new ExpFailFct(86).toString())
println ast.toListRep()
println ast.timeDistribution()

def evalUtils = new EvalUtilities()

def val = ((ISignedNumber)evalUtils.evaluate(new UserDefineFct("1/" + Double.MIN_VALUE).toString())).doubleValue()

println val