import faultTree.gates.AND
import faultTree.model.SAXModelParser
import org.xml.sax.helpers.AttributesImpl
import simulation.Element2

/**
 * Created by pierrj1 on 3/13/2015.
 */

object testGates {
  def main(args: Array[String]) {
    println("Hello, world!")

    val elem1 = new Element2("elem-1", "elem-1")
    val elem2 = new Element2("elem-2", "elem-2")

    val andGate = new AND("1234", new AttributesImpl())

    andGate.addChild(elem1)
    andGate.addChild(elem2)

    println()
//    println(andGate.output())

    println()
//    println(elem1.parent())
//    println(elem2.parent())

    println()
//    println(elem1.output())
//    println(elem2.output())

    val parser = new SAXModelParser("C:\\Users\\pierrj1\\workspace\\Thesis\\src\\test\\groovy\\test-resources\\test-model.xml")

    val tree = parser.parseDocument()

    println(tree)



  }
}