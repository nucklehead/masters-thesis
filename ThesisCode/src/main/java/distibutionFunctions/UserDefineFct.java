package distibutionFunctions;
//import java.lang.Math;
import org.matheclipse.core.eval.EvalEngine;
import org.matheclipse.parser.client.eval.DoubleEvaluator;

import java.util.HashSet;
import java.util.Set;


public class UserDefineFct extends Function {
	public String function;
	public boolean sign;

	public UserDefineFct(){
        function="t";
		sign=true;
	}
	public UserDefineFct(String func){
        Set<String> variables= new HashSet<String>();
        EvalEngine evalEngine = new EvalEngine();
        DoubleEvaluator.getVariables(func, variables);
        if (variables.size()<2)
            if (variables.contains("t") || variables.size() == 0)
                function= func;
            else
                throw new IllegalArgumentException("Function is not a single variable.");
        else
            throw new IllegalArgumentException("Function is not time dependent.");
        evalEngine.parse(func);
		sign=true;
	}
	public int getSign(){
		return sign ? 1 : -1;
	}
	public String toString(){
		return function;
	}
}
