package distibutionFunctions;
//import java.lang.Math;
public class ConstFct extends Function {
	public double value;
	public boolean sign;
	
	public ConstFct(){
		value=1;
		sign=true;
	}
	public ConstFct(boolean Sign,double val){
		value=val;
		sign=Sign;
	}
	public int getSign(){
		return sign ? 1 : -1;
	}
	public String toString(){
		return "("+(getSign()*value)+")";
	}
//	public double evaluateAt(double t){
//		return Math.exp(-lambda*t);
//	}
//	public double[] evaluate(double[] t){
//		double[] P=new double[t.length];
//		for(int i=0; i<t.length;i++)
//			P[i]=Math.exp(-lambda*t[i]);
//		return P;
//	}
}
