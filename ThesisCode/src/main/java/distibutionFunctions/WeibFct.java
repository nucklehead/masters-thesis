package distibutionFunctions;
//import java.lang.Math;
public class WeibFct extends Function {
	public double lambda;
	public double k;
	public boolean sign;
	
	public WeibFct(){
		lambda=1;
		k=1;
		sign=true;
	}
	public WeibFct(boolean Sign,double L,double K){
		lambda=L;
		sign=Sign;
		k=K;
	}
	public int getSign(){
		return sign ? 1 : -1;
	}
	public String toString(){
        return "("+getSign()+")*Exp[-(t/"+lambda+")^"+k+"]" /*"Times["+getSign()+",Exp[Times[Power[Times["+1/lambda+",t],"+ k +"], -1]]]"*/;

	}
//	public double evaluateAt(double t){
//		return Math.exp(-lambda*t);
//	}
//	public double[] evaluate(double[] t){
//		double[] P=new double[t.length];
//		for(int i=0; i<t.length;i++)
//			P[i]=Math.exp(-lambda*t[i]);
//		return P;
//	}
}
