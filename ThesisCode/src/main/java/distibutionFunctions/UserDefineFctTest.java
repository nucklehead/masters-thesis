package distibutionFunctions;

/**
 * Created by pierrj1 on 7/29/2014.
 */

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class UserDefineFctTest {
    Function linearF;
    Function sinF;
    Function compoundF;
    Function multivariableF;
    Function ERFguiny;
    Operation functionList;
    @Before
    public void setup() {
        linearF = new UserDefineFct("2t");
        sinF = new UserDefineFct("Sin[t]");
        compoundF = new UserDefineFct("Cos[t]+Sin[Log[t]]");
        ERFguiny = new UserDefineFct("Erf[.05t]");
    }
    @Test
    public void linearFunct(){
        functionList = new Operation();
        functionList.addFunction(true, linearF);
        double value = functionList.evaluateAt(10);
        assertEquals("2(10) is 20", 20.0, value, 0);
    }

    @Test
    public void sinFunct(){
        functionList = new Operation();
        functionList.addFunction(true, sinF);
        double value = functionList.evaluateAt(10);
        assertEquals("Sin(10) is -0.544021", -0.544021,value, 0.000001);
    }

    @Test
    public void compoundFunct(){
        functionList = new Operation();
        functionList.addFunction(true, compoundF);
        double value = functionList.evaluateAt(10);
        assertEquals("cos(10)+sin(log(10)) is -0.0950912", -0.0950912,value, 0.000001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setMultivariableFunct(){
        functionList = new Operation();
        multivariableF = new UserDefineFct("Sin[x]+Cos[y]");
    }
    @Test(expected = IllegalArgumentException.class)
    public void setMultivariableFunct2(){
        functionList = new Operation();
        multivariableF = new UserDefineFct("Sin[t]+Cos[y]");
    }
    @Test
    public void ERFFunct(){
        functionList = new Operation();
        functionList.addFunction(true, ERFguiny);
        double value = functionList.evaluateAt(10);
        assertEquals("Erf(.05(10))) is 0.520500", 0.520500,value, 0.000001);
    }
    @Test
    public void myERFFunct(){
        ERFguiny = new UserDefineFct("ErfSym[.05t]");
        functionList = new Operation();
        functionList.addFunction(true, ERFguiny);
        double value = functionList.evaluateAt(10);
        assertEquals("ErfSym(.05(10))) is 0.520500", 0.520500,value, 0.000001);
    }

}
