
package distibutionFunctions;

import org.matheclipse.core.eval.EvalUtilities;
import org.matheclipse.core.expression.F;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.core.interfaces.ISignedNumber;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantReadWriteLock;


public class Operation {
	protected List<List<Function>> op;
	protected List<Boolean> signs;
	public int length;
	volatile Double result;
    private final ReentrantReadWriteLock RWLock = new ReentrantReadWriteLock();

	public Operation(){
        F.initSymbols();
//        SystemNamespace.DEFAULT.add("WSNreliability.symJaFunctions.ErfSym");
        op= new ArrayList<List<Function>>();
        signs= new ArrayList<Boolean>();
        length=0;
	}
	public void addFunction( boolean sign, Function f){
		List<Function> tmp= new ArrayList<Function>();
		tmp.add(f);
		op.add(tmp);
		signs.add(sign);
	}
	public void addFunctions(boolean sign, List<Function> fs){
		op.add(fs);
		signs.add(sign);
	}
	public double evaluateAt(final double t){
		//System.out.println("Calculating at time: "+t);
		//System.out.print('\b');
		//System.out.print(t);
		result=0.0;
		
		//CEILING((LOG(size+1)+1))
		final int nThreads=(int) Math.ceil((Math.log(op.size() + 1) + 1) / Math.log(2) * 4);
		
        ExecutorService evalThread = Executors.newFixedThreadPool(nThreads);
        for (int i=0; i<op.size(); i++){
        	final int index=i;
	        evalThread.execute(new Runnable() {
		        public void run() {
		        	int myInt = index;
                    EvalUtilities solver = new EvalUtilities();
                    try {
                        solver.evaluate("t=" + t);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    double ans=1;
		            for (final Function express: op.get(myInt)){
                        try {
                            IExpr exp = solver.evaluate(express.toString());
                            while(exp.isAST()){
                                exp=solver.evaluate(exp);
                            }
                            ans *= ((ISignedNumber) exp).doubleValue();
                        }
                        catch (Exception e) {
                            System.out.println(express);
                            System.out.println();
                            try {
                                System.out.println(solver.evaluate(express.toString()));
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                            System.out.println(e);
                            System.exit(-1);
                        }
	            	}
		            try{
		            	RWLock.writeLock().lock();
			        	result+=(signs.get(myInt)? 1: -1)*ans;
		            }
		            catch(Exception e){
		            	System.out.println("Well guess I am the prob");
		            	System.out.println(e.toString());
		            }
		            finally{
		            	RWLock.writeLock().unlock();
		            }
		        }
		    });	
	    }
        evalThread.shutdown();
		while(!evalThread.isTerminated());
		return result;
	}
	public double[] evaluate(double[] t){
		double[] P=new double[t.length];
		//System.out.print("Calculating: "+op.get(0)+"\\");
		for(int i=0; i<t.length;i++){
			P[i]+=evaluateAt(t[i]);
		}
		return P;
	}
	@Override
	public String toString() {
		String representation="";
		for (int i=0;i< op.size(); i++){
			representation+=signs.get(i)? "+(": "-(";
			for (Function express: op.get(i))
				representation+=express;
			representation+=")\n";
		}
			
		return representation;
	}
	
}
