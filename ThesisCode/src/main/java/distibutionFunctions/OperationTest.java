package distibutionFunctions;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class OperationTest {

	@Test
	public void testEvaluateAt() {
		Operation tester = new Operation();
		tester.addFunction(true, new UserDefineFct( "t^2*t^2-1"));
		double d=tester.evaluateAt(3);
		assertEquals(Double.valueOf(d).toString(), "80.0");
		d=tester.evaluateAt(4);
        assertEquals(Double.valueOf(d).toString(), "255.0");
        tester = new Operation();
        tester.addFunction(true, new UserDefineFct("1-Exp[-t]"));
        d=tester.evaluateAt(3);
        assertEquals(Double.valueOf(d).toString(), "0.950212931632136");
	}
	@Test
	public void testParselimit() {
		Operation tester = new Operation();
		double d;
		tester.addFunction(true, new UserDefineFct("1t"));
		while(true){
			try{
				tester.addFunction(true, new UserDefineFct("(Exp[-t])"));
				d=tester.evaluateAt(3);
				if(tester.length>23191*2){
					System.out.println("Success");
					System.out.println("Length of expression: \n"+tester.length);
					break;
				}
			}
			catch(StackOverflowError e){
				System.out.println("Max length of expression: \n"+tester.length);
				break;
			}
		}
	}
}
