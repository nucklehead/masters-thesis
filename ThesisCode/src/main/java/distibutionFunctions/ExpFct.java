package distibutionFunctions;
//import java.lang.Math;
public class ExpFct extends Function {
	public double lambda;
	public boolean sign;
	
	public ExpFct(){
		lambda=1;
		sign=true;
	}
	public ExpFct(boolean Sign,double L){
		lambda=L;
		sign=Sign;
	}
	public int getSign(){
		return sign ? 1 : -1;
	}
	public String toString(){
		return "(" + getSign()+")Exp[-"+lambda+"*t]";
	}

//	public double evaluateAt(double t){
//		return Math.exp(-lambda*t);
//	}
//	public double[] evaluate(double[] t){
//		double[] P=new double[t.length];
//		for(int i=0; i<t.length;i++)
//			P[i]=Math.exp(-lambda*t[i]);
//		return P;
//	}
}
