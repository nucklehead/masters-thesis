package distibutionFunctions;
import java.util.Random;


//class for different randoms
	public class RandomGen extends Random {
		private static final long serialVersionUID = 1L;
		// uniform, gaussian(center), exponential(lambda), geometric(probability), poisson(lambda), pareto(alpha), cauchy
		//http://introcs.cs.princeton.edu/java/stdlib/StdRandom.java.html
		public double distribute(char a, double low, double high, double center ){
			if (a=='u')
				return Math.abs(nextDouble() * (high - low) + low);
			else if(a=='g'){
				double res= Math.abs(nextGaussian() + center);
				while(res>high || res<low)
					res= Math.abs(nextGaussian() + center);
				return res;
			}
				
			else if(a=='e'){
				double lambda=-Math.log(.1)/high;
				int myboolint=nextBoolean()? 1:-1;
				double res= myboolint* Math.log(2 - 2 * nextDouble())/lambda+center;
				while(res>high || res<low)
					res= myboolint* Math.log(2 - 2 * nextDouble())/lambda+center;
				return res;
			}
			else if(a=='o')
				//under construction
				return (int) Math.ceil(Math.log(nextDouble()) / Math.log(1.0 - .5));
			else if(a=='p'){
				int k = 0;
				double p = 1.0;
				double L = Math.exp(-(high - low) / 2);
				do {
					k++;
					p *= nextDouble();
				} while (p >= L);
				return k-1;
			}
			else if(a=='a')
				//return -Math.log(1-Math.abs(nextDouble())*(high-low)+low)/1;
				return 0;
			else if(a=='c')
				return Math.tan(Math.PI * (nextDouble() - 0.5))*2/(high-low)+(high-low)/2;
			else 
				return 0;
		}
	}