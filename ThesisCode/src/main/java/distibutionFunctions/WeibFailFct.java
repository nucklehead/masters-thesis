package distibutionFunctions;

/**
 * Created by pierrj1 on 1/25/2015.
 */
public class WeibFailFct extends WeibFct{

    public WeibFailFct(){
        super(false, 1, 1);
    }

    public WeibFailFct(double L, double k){
        super(false, L, k);
    }

    @Override
    public String toString() {
        return "1+" + super.toString();
    }

}
