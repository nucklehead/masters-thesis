package distibutionFunctions;

import org.codehaus.jackson.annotate.JsonValue;
import org.matheclipse.core.eval.EvalEngine;
import org.matheclipse.core.expression.AST;
import org.matheclipse.core.interfaces.IExpr;

import java.util.HashMap;
import java.util.Stack;
import java.util.regex.Pattern;


public abstract class Function {

    private static Pattern regex1 = Pattern.compile("^(.*?)\\[(.*?),(.*)\\]$");
    private static Pattern regex2 = Pattern.compile("^(.*?)\\[(.*?)\\]$");


    private static final HashMap<String, String> inverses;

    static
    {
        inverses = new HashMap<String, String>();
        inverses.put("ArcCos" , "Cos[" );
        inverses.put("ArcCot" , "Cot[" );
        inverses.put("ArcCsc" , "Csc[" );
        inverses.put("ArcSec" , "Sec[" );
        inverses.put("ArcSin" , "Sin[" );
        inverses.put("ArcTan" , "Tan[" );
        inverses.put("Cos" , "ArcCos[" );
        inverses.put("Cot" , "ArcCot[" );
        inverses.put("Csc" , "ArcCsc[" );
        inverses.put("D" , "Int[" );
        inverses.put("Diff" , "Integrate[" );
        inverses.put("Erf" , "InverseErf[" );
        inverses.put("Exp" , "Log[" );
        inverses.put("Int" , "D[");
        inverses.put("Integrate", "Diff[" );
        inverses.put("Log" , "Exp[" );
        inverses.put("Plus" , "Plus[ ,-" );
        inverses.put("Power" , "Log10[]/Log10[" );
        inverses.put("Sec" , "ArcSec[" );
        inverses.put("Sin" , "ArcSin[" );
        inverses.put("Sqrt" , "Power[ ,2" );
        inverses.put("Tan" , "ArcTan[" );
        inverses.put("Times" , "Times[ ,1/");
    }

    public abstract int getSign();
    @JsonValue
	public abstract String toString();

    public String toListRep(){
        IExpr function = new EvalEngine().parse(toString());
        return toListRep(function);
    }

    private String toListRep(IExpr function){
        String listRep = "";
        if (function.isAST()) {
            listRep += function.getAt(0) + "[";
                for(int i = 1; i< ((AST)function).size(); i++) {
                    listRep += toListRep(function.getAt(i)) + ",";
                }
            listRep = listRep.replaceFirst(",$", "") + "]";
            }
            else
                listRep = function.toString();
       return listRep;
    }

    public String timeDistribution() throws Exception {
        String function = toListRep();
        return inverseTraverse(function, "prob");
    }

    private String inverseTraverse(String function, String accumulator) throws Exception {

        if(!getArgumentNumber(function))
            throw new Exception("Invalid function format. Cannot find inverse");


        String[] splitFiunc = getArguments(function).split("//");
        if(getArgumentNumber(splitFiunc[1])){
            return inverseTraverse(splitFiunc[1], getInverse(splitFiunc[0] ,  accumulator ,  splitFiunc.length==3?splitFiunc[2]:""));
        }
        else if(getArgumentNumber(splitFiunc[2])){
            return inverseTraverse(splitFiunc[2], getInverse(splitFiunc[0] ,  accumulator ,  splitFiunc[1]));
        }
        else{
           return getInverse(splitFiunc[0] ,  accumulator ,  splitFiunc[1]);
        }
    }

    private String getInverse(String function, String argument1, String argument2){
        if(argument2.contains("prob")){
            String inverse = inverses.get(function) + argument1 + "]";
            return inverse.substring(0, inverse.indexOf('[')+1) + argument2 + inverse.substring(inverse.indexOf('[')+1);
        }
        else if(argument1.contains("prob")){
            String inverse = inverses.get(function) + argument2 + "]";
            return inverse.substring(0, inverse.indexOf('[')+1) + argument1 + inverse.substring(inverse.indexOf('[')+1);
        }
        return null;
    }

    private boolean getArgumentNumber(String function){
        return regex1.matcher(function).matches() || regex2.matcher(function).matches();
    }

    private String getArguments(String function){
        Stack<String> stack = new Stack<String>();
        String name = "";
        String argument1 = "";
        String argument2 = "";
        String tmp = "";
        for (char c: function.toCharArray()){
            if(c == '[') {
                if(stack.size()==0) {
                    name = tmp;
                    tmp = "";
                }
                else
                    tmp += c;
                stack.push("");
            }
            else if(c == ',') {
                if(stack.size()==1) {
                    argument1 = tmp;
                    tmp = "";
                }
                else
                    tmp += c;
            }
            else if(c == ']'){
                if(stack.size()==1) {
                    argument2 = tmp;
                    tmp = "";
                }
                else
                    tmp += c;
                stack.pop();
            }
            else
                tmp += c;
        }

        return name + "//" + argument1 + "//" +argument2;
    }

    private String toListString(){
        return null;
    }


//	public double evaluateAt(double t);
//	public double[] evaluate(double[] t);

}

