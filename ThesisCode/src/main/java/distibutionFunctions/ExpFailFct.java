package distibutionFunctions;

/**
 * Created by pierrj1 on 1/25/2015.
 */
public class ExpFailFct extends ExpFct{

    public ExpFailFct(){
        super(false, 1);
    }

    public ExpFailFct(double L){
        super(false, L);
    }

    @Override
    public String toString() {
        return "1+" + super.toString();
    }
}
