package simulation;

/**
 * Created by pierrj1 on 1/28/2015.
 */
public class ExperimentResults {
    public boolean success = false;
    public float failiureTime = Float.POSITIVE_INFINITY;
    public float idleTime = 0;
    public float cost = 0;

    void plus(ExperimentResults other){
        idleTime += other.idleTime;
        cost += other.cost;
    }
}
