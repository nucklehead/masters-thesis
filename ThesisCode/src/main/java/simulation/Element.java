package simulation; /**
 * Created by pierrj1 on 1/10/2015.
 */
import com.mongodb.DBObject;
import distibutionFunctions.ConstFct;
import distibutionFunctions.Function;
import distibutionFunctions.UserDefineFct;
import faultTree.model.Node;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import scala.collection.immutable.List;

import java.io.IOException;
import java.util.Random;


/**
 * The type Element.
 */
public class Element {
    /**
     * The Id.
     */
    public String id;
    /**
     * The Name.
     */
    public String name;
    /**
     * The Failiure funct.
     */
    public Function failiureFunct;
    /**
     * The Repair funct.
     */
    public Function repairFunct;


    /**
     * The Data size.
     */
    public int dataSize;
    /**
     * The Backup cost.
     */
    public float backupCost;

    /**
     * The Cwo.
     */
    public float cwo;
    /**
     * The Cho.
     */
    public float cho;
    /**
     * The Cwh.
     */
    public float cwh;

    /**
     * The Co.
     */
    public float co;
    /**
     * The Ch.
     */
    public float ch;
    /**
     * The Cw.
     */
    public float cw;

    /**
     * The Dh.
     */
    public float dh;
    /**
     * The Dw.
     */
    public float dw;

    /**
     * The State.
     */
    public State state;

    /**
     * The Tr.
     */
    @JsonSerialize(using = FloatSerializer.class)
    public Float tr, /**
     * The Tf.
     */
    tf, /**
     * The Tf original.
     */
    tf_original, /**
     * The To.
     */
    to, /**
     * The Th.
     */
    th, /**
     * The Tw.
     */
    tw;

    /**
     * The Time in operation.
     */
    public Float timeInOperation = 0F;
    /**
     * The Time in warm.
     */
    public Float timeInWarm = 0F;
    /**
     * The Time in hot.
     */
    public Float timeInHot = 0F;
    /**
     * The Time in failed.
     */
    public Float timeInFailed = 0F;


    /**
     * The enum State.
     */
    public enum State{
        /**
         * Warm state.
         */
        WARM, /**
         * Hot state.
         */
        HOT, /**
         * Operational state.
         */
        OPERATIONAL, /**
         * Failed state.
         */
        FAILED, /**
         * Wartofail state.
         */
        WARTOFAIL, /**
         * Hottofail state.
         */
        HOTTOFAIL, /**
         * Optofail state.
         */
        OPTOFAIL;

        private static final Random RANDOM = new Random();

        /**
         * Random state.
         *
         * @return the state
         */
        public static State random()  {
            return State.values()[RANDOM.nextInt(2)];
        }
    }

    /**
     * Instantiates a new Element.
     *
     * @param object the object
     */
    public Element(DBObject object){
        id = object.get("id").toString();

        name = object.get("name").toString();

        cwo = Float.parseFloat(object.get("cwo").toString());
        cho = Float.parseFloat(object.get("cho").toString());
        cwh = Float.parseFloat(object.get("cwh").toString());

        co = Float.parseFloat(object.get("co").toString());
        ch = Float.parseFloat(object.get("ch").toString());
        cw = Float.parseFloat(object.get("cw").toString());

        dh = Float.parseFloat(object.get("dh").toString());
        dw = Float.parseFloat(object.get("dw").toString());
        failiureFunct = new UserDefineFct(object.get("failiureFunct").toString());
        repairFunct = new UserDefineFct(object.get("repairFunct").toString());

        state = State.valueOf(object.get("state").toString());

        tf = Float.parseFloat(object.get("tf").toString());
        tf_original = Float.parseFloat(object.get("tf_original").toString());

        tr = Float.parseFloat(object.get("tr").toString());
        to = Float.parseFloat(object.get("to").toString());
        th = Float.parseFloat(object.get("th").toString());
        tw = Float.parseFloat(object.get("tw").toString());


        timeInOperation = Float.parseFloat(object.get("timeInOperation").toString());
        timeInWarm = Float.parseFloat(object.get("timeInWarm").toString());
        timeInHot = Float.parseFloat(object.get("timeInHot").toString());
        timeInFailed = Float.parseFloat(object.get("timeInFailed").toString());

        dataSize = Integer.parseInt(object.get("dataSize").toString());
        backupCost = Float.parseFloat(object.get("backupCost").toString());



//        id = ++count;
    }

    /**
     * Instantiates a new Element.
     */
    public Element(){
        name = "";

        cwo = 0;
        cho = 0;
        cwh = 0;

        co = 0;
        ch = 0;
        cw = 0;

        dh = 1;
        dw = 1;
        failiureFunct = new UserDefineFct(Double.MIN_VALUE + "*t");
        repairFunct = new UserDefineFct(Double.MIN_VALUE + "*t");
        state = State.OPERATIONAL;

        tf = 0F;
        tf_original = Float.POSITIVE_INFINITY;
        tr = 0F;

        to = 0F;
        th = 0F;
        tw = 0F;


        dataSize = 0;
        backupCost = 0;

//        id = ++count;
    }

    /**
     * Instantiates a new Element.
     *
     * @param cWO the c wo
     * @param cHO the c ho
     * @param cWH the c wh
     * @param cO  the c o
     * @param cH  the c h
     * @param cW  the c w
     * @param dH  the d h
     * @param dW  the d w
     * @param F   the f
     */
    Element(float cWO, float cHO, float cWH,
            float cO, float cH, float cW,
            float dH, float dW,
            Function F){
        this();
        cwo = cWO;
        cho = cHO;
        cwh = cWH;
        co = cO;
        ch = cH;
        cw = cW;
        dh = dH;
        dw = dW;
        failiureFunct = F;
    }

    @Override
    public String toString() {
        return "Element{" +
                "id=" + id +
                ", state=" + state +
                ", tf=" + tf +
                ", to=" + to +
                ", th=" + th +
                '}';
    }

    /**
     * The type Float serializer.
     */
    public static class FloatSerializer extends JsonSerializer<Float> {
        @Override
        public void serialize(Float aFloat, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(aFloat.toString());
        }
    }
}
