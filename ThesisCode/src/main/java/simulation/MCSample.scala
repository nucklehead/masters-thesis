package simulation

import Database.MongoController
import faultTree.gates.SPARE
import faultTree.model.Node


import distibutionFunctions.UserDefineFct
import org.matheclipse.core.eval.EvalUtilities
import org.matheclipse.core.interfaces.ISignedNumber

import scala.util.Random

/**
 * Created by pierrj1 on 3/3/2015.
 */
class MCSample(val index: Int,  var results: SimulationResults) extends Thread( new Runnable {
  override def run(): Unit = {
    val elems: List[Element2] = model.getAllElements
    val failureFunctions = elems.map(elem => new UserDefineFct(elem.failureFunction(index)).timeDistribution())
    val repairFunctions = elems.map(elem => new UserDefineFct(elem.repairFunction(index)).timeDistribution())

    val randList = getRandomGenerators(elems.size)
    val randList2 = getRandomGenerators2(elems.size)

    for (((elem, func), rand) <- elems zip failureFunctions zip randList) setTime(elem, func, rand)
    for (((elem, func), rand) <- elems zip repairFunctions zip randList2) setRepairTime(elem, func, rand)

    val timelineEvents = elems.map(elem => new TimeLineEvent(index, elem, elem.setStateTo("FAILED", index), elementFailureEvent,elem.getFailureTime(index)) )
    val backupEvents = model.getAll(backupSpareFilter).flatMap(backupSpareToEvents)

    val switchFailEvents = model.getAll(fdmSpareFilter).map(spare => new TimeLineEvent(index, spare, spare.asInstanceOf[SPARE].setSwitchStateTo("FAILED", index), switchFailureEvent,spare.asInstanceOf[SPARE].getSwitchFailureTime(index)))

    val currentTimeLine = timeLine(index)
    currentTimeLine.events = timelineEvents:::backupEvents:::switchFailEvents
    currentTimeLine.sort()
    val result = currentTimeLine.execute(index)
    result.success = !model.output(index)
    if(result.success) result.failiureTime = missionTime
    elems.foreach(_.sumarizeResults(index, result))
    results.synchronized{results.update(result)}

    sampleCList(index).clear()
    removeSampleCList(index)
    removeRepairBucket(index)
    removeTimeLineEvent(index)
  }

  def backupSpareToEvents(spareNode: Node): List[TimeLineEvent] = {
    spareNode.asInstanceOf[SPARE].backupFrequency match {
      case freqlist: List[Float] => freqlist.map(fraq => new TimeLineEvent(index, spareNode, spareNode.asInstanceOf[SPARE].backup(index), backupEventName, missionTime*fraq))
      case null => List()
      case freq: AnyRef => (for(fraq <- freq.asInstanceOf[Float] until 1 by freq.asInstanceOf[Float]) yield new TimeLineEvent(index, spareNode, spareNode.asInstanceOf[SPARE].backup(index), backupEventName, missionTime*fraq.toFloat)).toList
    }
  }

  def backupSpareFilter(node: Node): Boolean = {
    node match {
      case nod: SPARE => nod.backupSystem
      case _ => false
    }
  }

  def fdmSpareFilter(node: Node): Boolean = {
    node match {
      case nod: SPARE => nod.switchMechanism == "imperfect"
      case _ => false
    }
  }

  val solver = new EvalUtilities(true, false)

  def setTime(elem: Element2, funct: String, rand: Random) = {
    solver.evaluate("prob =" + rand.nextDouble())
    elem.setOriginalFailiureTime(index, solver.evaluate(funct).asInstanceOf[ISignedNumber].doubleValue().toFloat)
  }

  def setRepairTime(elem: Element2, funct: String, rand: Random) = {
    solver.evaluate("prob =" + rand.nextDouble())
    elem.setRepairTime(index, solver.evaluate(funct).asInstanceOf[ISignedNumber].doubleValue().toFloat)
  }

})
{

}
