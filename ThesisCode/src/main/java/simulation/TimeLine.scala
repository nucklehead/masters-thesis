package simulation

/**
 * Created by pierrj1 on 4/2/2015.
 */
class TimeLine{
  var events: List[TimeLineEvent] = List()
  def addEvent(event: TimeLineEvent): Unit ={
    events = events :+event
    sort()
  }
  def sort(): Unit={
    events = events.sortWith((e1: TimeLineEvent, e2:TimeLineEvent) => e1.time < e2.time)
  }

  def updateTime(id: String, time: Float): Unit ={
    val event = events.find( (event) => event.node.id == id )
    if(event.nonEmpty)
      event.get.time = time

    sort()
  }

  def shiftTime(currentTime: Float, time: Float): Unit ={
    events.foreach( (event) =>
      if (event.time > currentTime) event.time += time
    )
  }

  def trash(id: String, time: Float): Unit ={
    val event = events.find( (event) => event.node.id == id ).get
    events = events.slice(0, events.indexOf(event)):::events.slice(events.indexOf(event)+1, events.size)
    sort()
  }

  def getTime(id: String): Double ={
    events.find( (event) => event.node.id == id ).get.time
  }

  override def toString: String ={
    events.toString()
  }

  def execute(sampleIndex: Int): ExperimentResults ={
    var result: ExperimentResults = new ExperimentResults

    var lastDown:Float = 0
    var down = false
    val iterator = events.iterator
    while(iterator.hasNext){
      val event = iterator.next()
      if(down) result.idleTime += event.time - lastDown
      event.trigger(result)
      if(model.output(sampleIndex)) {
        lastDown = event.time
        down = true
      }
    }
    if(down && lastDown < missionTime) result.idleTime += missionTime - lastDown
    result
  }

}
