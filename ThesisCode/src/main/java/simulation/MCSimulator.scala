package simulation

import Database.MongoController
import WebAppSample.{SimulateTask, HomePage}
import faultTree.model.{Root, SAXModelParser}
import scala.math.ceil


/**
 * Created by pierrj1 on 4/9/2015.
 */
object  MCSimulator {
  var modelFile: String = ""
  var concurentThread: Int = 1
  var threshold: Float = 0.001F

  var results = new SimulationResults()
  var maxProgress = 0;

  lazy val task = HomePage.taskProgressView.getTasks.get(HomePage.taskProgressView.getTasks.size()-1).asInstanceOf[SimulateTask]

  def main(args: Array[String]) {
    missionTime = args(0).toFloat
    threshold = args(1).toFloat
    concurentThread = args(2).toInt
    modelFile = System.getProperty("model")
    run()
  }

  def initialize(time: String, thresh: String, thread: String, file: String): Unit ={
    missionTime = time.toFloat
    threshold = thresh.toFloat
    concurentThread = thread.toInt
    modelFile = file
    results = new SimulationResults()
  }

  def run(){
    modelParser = new SAXModelParser(modelFile)
    //note: This is only for testing purposes
//    MongoController.populateDatabase()

//    runWithNThread(20)
    while(results.standardDeviation > threshold){
      runWithNThread(concurentThread)
    }
    task.update(maxProgress,maxProgress)
    println(results)
  }
  def runWithNThread(n: Int): Unit ={
    var samples: List[MCSample] = List()
    for(index <- 0 to n -1){
      samples = samples:::new MCSample(index, results)::Nil
    }
    for(index <- 0 to n -1){
      samples(index).run()
    }
    for(index <- 0 to n -1) {
      samples(index).join()
      if(results.iterations == 2) {
        maxProgress = ceil(results.standardDeviation).toInt
        task.update(0,maxProgress)
      }
      else if(results.iterations > 2) task.update(maxProgress - ceil(results.standardDeviation).toInt,maxProgress)
    }
  }
}
