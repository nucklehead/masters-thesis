package simulation

import faultTree.gates.{Gate, SPARE}
import faultTree.model.Node
import scala.math.min
import scala.math.max

/**
 * Created by pierrj1 on 3/11/2015.
 */
class Element2(idc: String, namec: String) extends Node {
  override val id = idc
  override val name = namec
  def output(sampleIndex: Int):Boolean ={
    sampleCList(sampleIndex).getElementState(id) == "FAILED"
  }
  // after the change, need to modify other variables for cost, tf,
  override def setStateTo(state: String, sampleIndex: Int)(result: ExperimentResults)(time: Float): Unit ={
    val curState = sampleCList(sampleIndex).getElementState(id)
    val elem = sampleCList(sampleIndex).getElement(id)
    (curState, state) match{
      case ("WARM", "HOT") =>{
        if (elem.tf.isInfinite) elem.tf = elem.tf_original
        else elem.tf = (elem.tf - time) * elem.dw / elem.dh + time
        result.cost += elem.cwh
        elem.th = time
        elem.timeInWarm += time - elem.tw
      }
      case ("WARM", "OPERATIONAL") => {
        if (elem.tf.isInfinite) elem.tf = elem.tf_original
        else elem.tf = (elem.tf - time) * elem.dw  + time
        result.cost += elem.cwo
        elem.to = time
        elem.timeInWarm += time - elem.tw
      }
      case ("HOT", "OPERATIONAL") =>{
        if (elem.tf.isInfinite) elem.tf = elem.tf_original
        else elem.tf = (elem.tf - time) * elem.dw  + time
        result.cost += elem.cho
        elem.to = time
        elem.timeInHot += time - elem.th
      }
      case ("FAILED", "FAILED") =>
      case (_, "FAILED") =>{
        timeLine(sampleIndex).trash(id, time)
//        sampleCList(sampleIndex).setElementState(id, state)
        val repairTime = sampleCList(sampleIndex).getElementRepairTime(id)
        if(repairTime != Float.PositiveInfinity)
          timeLine(sampleIndex).addEvent(new TimeLineEvent(sampleIndex, this, setStateTo(curState, sampleIndex), elementRepairEvent,time + repairTime))
        else if(model.output(sampleIndex) && result.failiureTime > time) result.failiureTime = time
        if(parent.isInstanceOf[SPARE]){
          if(parent.children.indexOf(this) == 0) parent.asInstanceOf[SPARE].replace(sampleIndex)(result)(time)
        }
        removeSelfFrom(sampleIndex)

        elem.tf = time
        curState match {
          case "WARM" =>{
            elem.timeInWarm += time - elem.tw
          }
          case "HOT" =>{
            elem.timeInHot += time - elem.th
          }
          case "OPERATIONAL" =>{
            elem.timeInOperation += time - elem.to
          }
        }
      }
      //repair
      case ("FAILED", _) =>{
        timeLine(sampleIndex).trash(id, time)
        timeLine(sampleIndex).addEvent(new TimeLineEvent(sampleIndex, this, setStateTo("FAILED", sampleIndex), elementFailureEvent, time + sampleCList(sampleIndex).getElementOriginalFailiureTime(id)))
        parent.asInstanceOf[Gate]addRepairedChild(this, sampleIndex)

        elem.timeInFailed += time - elem.tf
        state match {
          case "WARM" => {
            elem.tw = time
          }
          case "HOT" => {
            elem.th = time
          }
          case "OPERATIONAL" => {
            elem.to = time
          }
        }
      }
    }
    elem.state = Element.State.valueOf(state)
//    sampleCList(sampleIndex).setElementState(id, state)
//    sampleCList(sampleIndex).setElementFailiureTime(id, elem.tf)
    sampleCList(sampleIndex).setElement(elem)

    timeLine(sampleIndex).updateTime(id, elem.tf)
  }
  def getState(sampleIndex: Int) ={
    sampleCList(sampleIndex).getElementState(id)
  }

  def getDataSize(sampleIndex: Int) ={
    sampleCList(sampleIndex).getElementDataSize(id)
  }

  def failureFunction(sampleIndex: Int) = sampleCList(sampleIndex).getElementFailiureFunct(id)
  def getFailureTime(sampleIndex: Int) = sampleCList(sampleIndex).getElementFailiureTime(id)

  def repairFunction(sampleIndex: Int) = sampleCList(sampleIndex).getElementRepairFunct(id)
  def getRepairTime(sampleIndex: Int) = sampleCList(sampleIndex).getElementRepairTime(id)

  def setOriginalFailiureTime(sampleIndex: Int, time: Float): Unit ={
    sampleCList(sampleIndex).setElementOriginalFailiureTime(id, time)
    getState(sampleIndex) match{
      case "WARM" => sampleCList(sampleIndex).setElementFailiureTime(id, time / sampleCList(sampleIndex).getElementDw(id))
      case "HOT" => sampleCList(sampleIndex).setElementFailiureTime(id, time / sampleCList(sampleIndex).getElementDh(id))
      case _ => sampleCList(sampleIndex).setElementFailiureTime(id, time)
    }
  }

  def setRepairTime(sampleIndex: Int, time: Float): Unit ={
    sampleCList(sampleIndex).setElementRepairTime(id, time)
  }

  def removeSelfFrom(sampleIndex: Int) = {
//    sampleCList(sampleIndex).removeElement(id
//    parent.removeChild(this)
    addToRepairBucket(sampleIndex, this)
  }

  def sumarizeResults(sampleIndex: Int, result: ExperimentResults): Unit ={
    val elem = sampleCList(sampleIndex).getElement(id)
    result.cost += elem.timeInWarm * sampleCList(sampleIndex).getElementCw(id) +
      elem.timeInHot * sampleCList(sampleIndex).getElementCh(id) +
      elem.timeInOperation * sampleCList(sampleIndex).getElementCo(id)
  }

  def addBackupCost(sampleIndex: Int, result: ExperimentResults): Unit ={
    result.cost += sampleCList(sampleIndex).getBackupCost(id)
  }
}
