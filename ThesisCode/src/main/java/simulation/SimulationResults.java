package simulation;

/**
 * Created by pierrj1 on 1/28/2015.
 */
public class SimulationResults {
    public int iterations = 0;
    public int failed = 0;
    public float expectedFailiureTime = Float.POSITIVE_INFINITY;
    public float expectedIdleTime = 0;
    public float expectedCost = 0;
    public float standardDeviation = Float.POSITIVE_INFINITY;

    private float sysIdleTimeSum = 0;
    private float varianceSum = 0;
    private float cost = 0;
    private float sysFailTSum = 0;




    void update(ExperimentResults results){
        sysIdleTimeSum += results.idleTime;
        iterations++;
        cost += results.cost;

        sysFailTSum += results.failiureTime;
        int N = iterations;

        if(N>1) {
            expectedFailiureTime = sysFailTSum / N;
            expectedIdleTime = sysIdleTimeSum / N;
            varianceSum += Math.pow(results.idleTime - expectedIdleTime, 2);
            float variance = varianceSum / (N - 1);
            standardDeviation = (float) Math.sqrt(variance / N);
            if (!results.success) {
                failed++;
            }
//            logger.info("Simulation after " + iterations + " iterations.\n" +
//                    "Availability: " + (expectedFailT - expectedIdleT) / expectedFailT + "\n" +
//                    "Cost        : " + cost / iterations + "\n" +
//                    "Conv. Factor: " + currentConvFactor);
        }
        expectedCost = cost / iterations;

    }

    @Override
    public String toString() {
        String output ="";
        output += "__________________\n";
        output += "Simulation Results\n";
        output += "¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯\n";

        output += "Total runs           : " + iterations + "\n";
        output += "Failed               : " + failed + "\n";
        output += "Expected Failure Time: " + expectedFailiureTime + "\n";
        output += "Expected Idle Time   : " + expectedIdleTime + "\n";
        output += "Expected Cost        : " + expectedCost + "\n";
        output += "Convergence factor   : " + standardDeviation + "\n";


        return output;
    }
}
