
import Database.MongoController
import faultTree.model.{SAXModelParser, Root, Node}

import scala.util.Random

/**
 * Created by pierrj1 on 4/10/2015.
 */
package object simulation {

  var missionTime: Float = 0;

  private var uniqueSampleCList: Map[Int, MongoController] = Map()
  private var uniqueTimelineList: Map[Int, TimeLine] = Map()
  private var repairBucketList: Map[Int, List[Node]] = Map()


  private val randomGenerators = null
  private val randomGenerators2 = null

  var modelParser: SAXModelParser = null
  lazy val model: Root = modelParser.parseDocument().normalize()

  val backupEventName: String = "Backup element"
  val elementFailureEvent: String = "Set element state to FAILED"
  val elementRepairEvent: String = "Set element state to OPERATIONAL"
  val switchFailureEvent: String= "Set switch state to FAILED"

  def getRandomGenerators(numRand: Int): List[Random] ={
    randomGenerators match {
      case null => List.fill(numRand)(new Random())
      case _ => randomGenerators

    }
  }
  def getRandomGenerators2(numRand: Int): List[Random] ={
    randomGenerators2 match {
      case null => List.fill(numRand)(new Random())
      case _ => randomGenerators2

    }
  }
  def sampleCList(index: Int): MongoController = {
    uniqueSampleCList.getOrElse(index, null) match {
        case null => {
          uniqueSampleCList += index -> new MongoController()
          uniqueSampleCList(index)
        }
        case contrl: MongoController => contrl
      }
    }

  def removeSampleCList(index: Int): Unit = {
    uniqueSampleCList.getOrElse(index, null) match {
      case null => {
      }
      case contrl: MongoController => uniqueSampleCList += index -> null
    }
  }

  def timeLine(index: Int): TimeLine = {
    uniqueTimelineList.getOrElse(index, null) match {
      case null => {
        uniqueTimelineList += index -> new TimeLine()
        uniqueTimelineList(index)
      }
      case timeline: TimeLine => timeline
    }
  }

  def removeTimeLineEvent(index: Int): Unit = {
    uniqueTimelineList.getOrElse(index, null) match {
      case null => {
      }
      case timeline: TimeLine => uniqueTimelineList += index -> null
    }
  }

  def repairBucket(index: Int): List[Node] = {
    repairBucketList.getOrElse(index, null) match {
      case null => {
        repairBucketList += index -> List()
        repairBucketList(index)
      }
      case bucket: List[Node] => bucket
    }
  }

  def addToRepairBucket(index: Int, node: Node): Unit ={
    repairBucketList.getOrElse(index, null) match {
      case null => {
        repairBucketList += index -> List(node)
      }
      case bucket: List[Node] => repairBucketList += index -> (bucket :+ node)
    }
  }

  def removeRepairBucket(index: Int): Unit = {
    repairBucketList.getOrElse(index, null) match {
      case null => {
        repairBucketList += index -> List()
      }
      case bucket: List[Node] => repairBucketList += index -> null

    }
  }

}
