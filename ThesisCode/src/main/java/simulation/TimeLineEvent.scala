package simulation


/**
 * Created by pierrj1 on 3/30/2015.
 */

import faultTree.gates.SPARE
import faultTree.model.Node
import simulation.ExperimentResults

//This event can be
//SPARE.switch(time)
//Element.setStateTo(state) when failed
//Switch.setStateTo(state)
class TimeLineEvent(val sampleIndex: Int, val node: Node, var nodeMethod: (ExperimentResults)=>(Float)=> Unit, methodName:String,  var time: Float)  {
  def trigger(result: ExperimentResults) {
    if(time <= missionTime)
      nodeMethod(result)(time)
  }
  override def toString: String ={
    "Node: " +  node.id + "\n" +
    "Calls method: " +  methodName + "\n" +
    "at: " +  time + "s\n"
  }
}