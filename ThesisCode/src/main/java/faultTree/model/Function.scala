package faultTree.model

import org.matheclipse.core.eval.EvalUtilities
import org.matheclipse.core.interfaces.ISignedNumber

/**
 * Created by pierrj1 on 3/26/2015.
 */
class MyFunction(val expression: String) {
    def apply(number: Int): Float = {
      val solver = new EvalUtilities()
      solver.evaluate("value=" + number)
      solver.evaluate(expression).asInstanceOf[ISignedNumber].doubleValue().toFloat
    }
}
