package faultTree.model

/**
 * Created by pierrj1 on 3/10/2015.
 */
class LogicalValue(val booleanValue: Boolean) extends Node{
  override val name = booleanValue.toString
  override val id = ""
  override def output(sampleIndex: Int) = booleanValue
}
