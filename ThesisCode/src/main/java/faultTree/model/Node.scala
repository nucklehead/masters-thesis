package faultTree.model

import faultTree.gates.SPARE
import simulation.{ExperimentResults, Element2}

import scala.annotation.tailrec

/**
 * Created by pierrj1 on 2/25/2015.
 */
trait Node {
  val id: String
  val name: String
  var children: List[Node] = List()
  var parent: Node = null

  val childState: String = "OPERATIONAL"


  def addChild(child: Node) = {
    children = children ::: child::Nil
    child.parent = this
  }

  def removeChild(child: Element2)={
    children = children diff List(child):::child::Nil
  }

  def setStateTo(state: String, sampleIndex: Int)(result: ExperimentResults)(time: Float): Unit ={
    children.foreach(_.setStateTo(state, sampleIndex)(result)(time))
  }

  @tailrec
  final def depth(parentDepth: Int): Int = {
    if(parent == null ) parentDepth
    else parent.depth(parentDepth + 1)
  }
  //todo: this may be more efficient tailrec
  def getAllIds: List[String] = id::children.flatMap(_.getAllIds)
  def getAllElementIds: List[String] = this match {
    case node: Element2 => id :: children.flatMap (_.getAllElementIds  )
    case _ => children.flatMap (_.getAllElementIds )
  }

  def getAllElements: List[Element2] = this match {
    case node: Element2 => node :: children.flatMap (_.getAllElements )
    case _ => children.flatMap (_.getAllElements )
  }

  def getAll(function: (Node)=> Boolean): List[Node] = {
    if ( function(this) ) this :: children.flatMap( _.getAll(function) )
    else children.flatMap ( _.getAll (function) )
  }

  override def toString(): String = "\t" * depth(0) + name + ":::" + id + "\n" + children.map((child: Node) => child.toString()).mkString

  def output(sampleIndex: Int): Boolean
}
