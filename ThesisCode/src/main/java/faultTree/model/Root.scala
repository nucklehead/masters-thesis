package faultTree.model

import Database.MongoController
import faultTree.gates.SPARE

/**
 * Created by pierrj1 on 2/28/2015.
 */
class Root(idc: String, namec: String) extends Node{
  override val name = namec
  override val id = idc
  children = List()
  override def output(sampleIndex: Int): Boolean = children.head.output(sampleIndex)

  def normalize(): Root ={
    val listForm = getAll(_.isInstanceOf[SPARE])
    listForm.foreach{(spare: Node)=>
      spare.getAllElementIds.foreach((_id: String)=>MongoController.setAllElementState(_id,spare.childState))
      MongoController.setAllElementState(spare.children.head.id,spare.asInstanceOf[SPARE].active)
    }
    this
  }
}
