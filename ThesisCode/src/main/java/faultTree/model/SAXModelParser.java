package faultTree.model;
/**
 * Created by pierrj1 on 3/4/2015.
 */


import java.io.IOException;
import java.lang.reflect.Constructor;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import WebAppSample.*;
import WebAppSample.shapes.Element;
import WebAppSample.shapes.Pin;
import WebAppSample.shapes.SidePinGate;
import faultTree.gates.Gate;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import org.xml.sax.helpers.DefaultHandler;
import simulation.Element2;
import java.rmi.server.UID;

import com.sun.javafx.robot.impl.BaseFXRobot;


/**
 * The type Sax model parser.
 */
public class SAXModelParser extends DefaultHandler{
    //to maintain context
    private Root root;
    private Node curent;
    private Element parentShape;
    private Element childShape = null;
    private String fileLocation;

    private int count = 1;

    /**
     * The App view.
     */
    public Pane appView = null;
    private BaseFXRobot robot;

    private ProcessTask task;


    /**
     * Instantiates a new Sax model parser.
     *
     * @param fileLocation the file location
     */
    public SAXModelParser(String fileLocation){
        this.fileLocation =fileLocation;
    }

    /**
     * Parse document root.
     *
     * @return the root
     */
    public Root parseDocument() {

        //get a factory
        SAXParserFactory spf = SAXParserFactory.newInstance();
        try {

            //get a new instance of parser
            SAXParser sp = spf.newSAXParser();

            //parse the file and also register this class for call backs

            sp.parse(fileLocation, this);

        }catch(SAXException | ParserConfigurationException | IOException se) {
            se.printStackTrace();
        }
        if(appView !=null)
            task.update(count+3, count+3);
        return root;
    }

    /**
     * Iterate through the list and print
     * the contents
     */


    //Event Handlers
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //start
        if(qName.equalsIgnoreCase("model")) {
            System.out.println(attributes.getValue("text"));
            new Model();
            if(appView !=null) {
                task = (ProcessTask) HomePage.taskProgressView.getTasks().get(HomePage.taskProgressView.getTasks().size()-1);
                robot = new BaseFXRobot(appView.getScene());
            }
        }
        else if(qName.equalsIgnoreCase("root")) {
            root = new Root(new UID().toString(), attributes.getValue("text"));
            curent = root;
        }
        else {
            Node tmp = null;
            if (qName.equalsIgnoreCase("gate")) {
                Class<?> clazz = null;
                try {
                    clazz = Class.forName("faultTree.gates." + attributes.getValue("type"));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
                Constructor<?> ctor = null;
                try {
                    ctor = clazz.getConstructor(String.class, Attributes.class);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
                Object[] parameters = {new UID().toString(),attributes};
                try {
                    tmp = (Gate) ctor.newInstance(parameters);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            } else if (qName.equalsIgnoreCase("element")) {
                String id = attributes.getValue("id");
                String name = attributes.getValue("name");
                tmp = new Element2(id, name);
            }
            curent.addChild(tmp);
            curent = tmp;
        }
        loadVisuals(qName, attributes);

    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(!qName.equalsIgnoreCase("model")) {
            curent = curent.parent();
            if(appView !=null) {
                task.update(++count, count+2);
                if (parentShape.pinOut != null)
                    if (parentShape.pinOut.parent != null)
                        parentShape = (Element) parentShape.pinOut.parent.getParent();
            }
        }
    }

    private void loadVisuals(String qName, Attributes attributes){
        if(appView !=null && !qName.equalsIgnoreCase("model")){
            Class<?> clazz = null;
            try {
                clazz = Class.forName("WebAppSample.shapes." + attributes.getValue("type"));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                System.exit(-1);
            }
            Constructor<?> ctor = null;
            try {
                ctor = clazz.getConstructor();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                System.exit(-1);
            }
            try {
                childShape = (Element) ctor.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(-1);
            }
            FxPlatformExecutor.runOnFxApplicationAndWait(() -> {
                String name = attributes.getValue("name");
                if(attributes.getValue("id") != null)
                    name += "->" + attributes.getValue("id");
                DraggableObject draggableObject = new DraggableObject(name,
                        Double.parseDouble(attributes.getValue("translatex")), Double.parseDouble(attributes.getValue("translatey")),
                        appView.widthProperty().add(0), appView.heightProperty().add(0),
                        childShape, appView);
                if(name.equals("SPARE")){
                    new OptionsParser().updateInfo(draggableObject.info, name);
                    for(Object object: draggableObject.info.getItems()){
                        OptionsParser.Property property = ((OptionsParser.Property) object);
                        property.set(attributes.getValue(property.text.getText()));
                    }
                }
            });

            if(!qName.equalsIgnoreCase("root")){
                FxPlatformExecutor.runOnFxApplicationAndWait(() -> {
                    childShape.pinOut.fireEvent(new MouseEvent(MouseEvent.MOUSE_CLICKED, 0, 0, 0, 0, MouseButton.PRIMARY, 1, false, false, false, false, true, false, false, false, false, false, null));
                    Pin.waitForInit.acquireUninterruptibly();
                    if(parentShape.hasEmptyLeftPin())
                        ((SidePinGate)parentShape).pinSide.fireEvent(new MouseEvent(MouseEvent.MOUSE_CLICKED, 0, 0, 0, 0, MouseButton.PRIMARY, 1, false, false, false, false, true, false, false, false, false, false, null));
                    else
                        parentShape.pinIn.fireEvent(new MouseEvent(MouseEvent.MOUSE_CLICKED, 0, 0, 0, 0, MouseButton.PRIMARY, 1, false, false, false, false, true, false, false, false, false, false, null));


                    Pin.waitForInit.acquireUninterruptibly();
                });
            }
            else {
                HomePage.xmlRoot = (WebAppSample.shapes.Root) childShape;
            }
         parentShape = childShape;
        }
    }
}