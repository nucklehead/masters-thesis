package faultTree.gates.customGates

import faultTree.gates.Gate
import org.xml.sax.Attributes
import simulation.{Element, Element2}
import faultTree.model.{LogicalValue, Node}

/**
  * Created by pierrj1 on 3/8/2015.
  */
// Needs
class Custom(idc: String, attributes: Attributes) extends Gate{
  override val name = "FDEP"
  override val id = idc

  var arg1: String = attributes.getValue("arg1")
  var arg2: String = attributes.getValue("arg2")

   override def function(sampleIndex: Int)(child1: Node, child2: Node) = new LogicalValue(child1.output(sampleIndex) && child2.output(sampleIndex))
  override def output(sampleIndex: Int): Boolean= {
    if(children.head.output(sampleIndex)) children.head.output(sampleIndex)
    else children.tail.reduce(function(sampleIndex)).output(sampleIndex)
  }
 }
