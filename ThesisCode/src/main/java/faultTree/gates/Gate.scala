package faultTree.gates

import faultTree.model.Node
import scala.collection.JavaConverters._


/**
 * Created by pierrj1 on 2/26/2015.
 */
trait Gate extends Node {
  def function(sampleIndex: Int)(child1: Node, child2: Node): Node
  def output(sampleIndex: Int):Boolean ={
    children match {
      case head::tail =>
        children.reduce (function (sampleIndex) ).output (sampleIndex)
      case Nil => false
    }
  }

  def addRepairedChild(child: Node, sampleIndex: Int): Unit ={
    simulation.sampleCList(sampleIndex).setElementState(child.id, childState)
//    addChild(child)
  }
}
