package faultTree.gates

import faultTree.model.Node
import faultTree.model.LogicalValue
import org.xml.sax.Attributes

/**
 * Created by pierrj1 on 3/8/2015.
 */
class AND(idc: String, attributes: Attributes) extends Gate{
  override val name = "AND"
  override val id = idc
  override def function(sampleIndex: Int)(child1: Node, child2: Node) = new LogicalValue(child1.output(sampleIndex) && child2.output(sampleIndex))
}
