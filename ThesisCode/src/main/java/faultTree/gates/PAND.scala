package faultTree.gates

import faultTree.model.{LogicalValue, Node}
import org.xml.sax.Attributes
import simulation.repairBucket

import scala.annotation.tailrec

/**
  * Created by pierrj1 on 3/8/2015.
  */
class PAND(idc: String, attributes: Attributes) extends Gate{
  override val name = "PAND"
  override val id = idc
  var index: Int = 0
  def order : List[String] = getAllElementIds.tail
  def actualOrder: List[String] = repairBucket(index).map(_.id)
   override def function(sampleIndex: Int)(child1: Node, child2: Node) = new LogicalValue(true)
   override def output(sampleIndex: Int)={
     index = sampleIndex
     sorted(order, actualOrder)
   }

  @tailrec
  final def sorted(list: List[String], order: List[String] ): Boolean = {
    list match{
      case Nil => true
      case head::Nil => true
      case head::tail => {
        if (order.indexOf(head) > order.indexOf(tail.head)) false
        else
          sorted(tail, order)
      }
    }
  }
 }
