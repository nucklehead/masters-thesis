package faultTree.gates

import faultTree.model.{LogicalValue, Node}
import org.xml.sax.Attributes

/**
  * Created by pierrj1 on 3/8/2015.
  */
class XOR(idc: String, attributes: Attributes) extends Gate{
  override val name = "XOR"
  override val id = idc
   override def function(sampleIndex: Int)(child1: Node, child2: Node) = new LogicalValue(child1.output(sampleIndex) ^ child2.output(sampleIndex))
 }
