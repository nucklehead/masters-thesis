package faultTree.gates

import faultTree.model.{MyFunction, LogicalValue, Node}
import simulation.{ExperimentResults, Element2, sampleCList, timeLine}
import org.xml.sax.Attributes

/**
 * Created by pierrj1 on 3/8/2015.
 */
class SPARE(namec: String, idc: String, childrenc: List[Node], modec: String, activec: String,
            inspectionTimec: String, transferModec: String, transferTimec: String, switchMechanismc: String, switchIdc: String,
            backupSystemc: String, backupFrequencyc: String, backupDataFunctionc: String) extends Gate {
  override val name = namec
  override val id = idc

  children = childrenc
//  val mode: String = modec
  override val childState: String = modec
  val active: String = if(activec != null) activec else "OPERATIONAL"
  //the state of the active element
  val faultDetect: String = if (inspectionTimec != null) "inspection" else "automatic"
  /*Automatic, Inspection*/
  val inspectionTime: Double = if (inspectionTimec != null) inspectionTimec.toDouble else Double.PositiveInfinity
  val transferMode: String = transferModec
//  todo: implementcondition class
  /*Scheduled, condition = numSpares==2*/
  val transferTime: Double = if (transferTimec != null) transferTimec.toDouble else Double.PositiveInfinity
  val switchMechanism: String = switchMechanismc
  /*Perfect, Imperfect*/
  val switchId: String = switchIdc
  //keep witch elem with nodes
  val backupSystem: Boolean = if (backupSystemc != null) backupSystemc.toBoolean else false
  val freqList = """\[(.*)\]""".r
  val freqList2 = """List\((.*)\)""".r
  val freqSingle = """(.*)""".r
  val backupFrequency: AnyRef = backupFrequencyc match { /* ½, [ ¼, ½ …]*/
    case null => null
    case freqList(list) => list.split(",").toList.map(_.toFloat)
    case freqList2(list) => list.split(",").toList.map(_.toFloat)
    case freqSingle(freq) => freq.toFloat.asInstanceOf[AnyRef]
  }
  val backupDataFunction: MyFunction = if(backupDataFunctionc != null) new MyFunction(backupDataFunctionc) else new MyFunction("")
  var lastBackup: Double = 0.0


  def this(idc: String, attributes: Attributes) {
    this("SPARE", idc, List(), attributes.getValue("mode"), attributes.getValue("active"),
      attributes.getValue("inspec_time"), attributes.getValue("transf_mode"), attributes.getValue("transf_time"),
      attributes.getValue("switch"), attributes.getValue("switch_id"), attributes.getValue("backup"), attributes.getValue("backup_frequency"),
      attributes.getValue("backup_function"))
  }

  def this(namec: String, idc: String, modec: String, activec: String,
           inspectionTimec: String, transferModec: String, transferTimec: String, switchMechanismc: String, switchIdc: String,
            backupc: String, backupFrequencyc: String, backupFunctionc: String) {
    this(namec, idc, List(), modec, activec, inspectionTimec, transferModec, transferTimec, switchMechanismc, switchIdc,
      backupc, backupFrequencyc, backupFunctionc)
  }

  override def function(sampleIndex: Int)(child1: Node, child2: Node) = new LogicalValue(false)

  override def output(sampleIndex: Int): Boolean ={
    val head = getAll {
      case element: Element2 => element.getState(sampleIndex) == active
      case _ => false
    }
    if(head.nonEmpty) false
    else true
  }


  def replace(sampleIndex: Int)(result: ExperimentResults)(time: Float): Unit = {
    children match {
      case Nil =>

      case head::Nil =>

      case head::(head2:Element2)::tail => {
        val replacements = children.find {
          case element: Element2 => element.getState(sampleIndex) == childState
          case _ => false
        }
        if(replacements.nonEmpty)
        replacements.head.setStateTo(active, sampleIndex)(result)(time)
      }
      case head::(head2:SPARE)::tail => {
        if(!head2.children.head.output(sampleIndex)) {
          head2.children.head.setStateTo(active, sampleIndex)(result)(time)
          head2.replace(sampleIndex)(result)(time)
        }
      }
    }
  }

  override def setStateTo(state: String, sampleIndex: Int)(result: ExperimentResults)(time: Float): Unit ={
    children.head.setStateTo(state, sampleIndex)(result)(time)
  }

  def setSwitchStateTo(state: String, sampleIndex: Int)(result: ExperimentResults)(time: Float): Unit ={
    //todo allow switch repair
    sampleCList(sampleIndex).setElementState(switchId, state)
  }
  def getSwitchFailureTime(sampleIndex: Int): Float = {
    sampleCList(sampleIndex).getElementFailiureTime(switchId)
  }

  //returns time it takes to backup
  //todo: read backup to know what it does exactly
  def backup(sampleIndex: Int)(result: ExperimentResults)(time: Float): Unit = {
    lastBackup = time
    val by = backupDataFunction(children.head.asInstanceOf[Element2].getDataSize(sampleIndex))
    timeLine(sampleIndex).shiftTime(time, by)
    children.head.asInstanceOf[Element2].addBackupCost(sampleIndex, result)
  }

  //
  def swicth(sampleIndex: Int)(result: ExperimentResults)(time: Float) = {
//    todo:add ->this is only ot happen on trenasfer mode condition
//    todo:should be called everytime the object states has changed (remember elements)
    transferTime match {
      case Double.PositiveInfinity =>  //todo: implement other conditions
      case _ => {
        if(time == transferTime) children.foreach(_.setStateTo(active, sampleIndex)(result)(time))
      }
    }
  }

  override def addRepairedChild(child: Node, sampleIndex: Int): Unit ={
    children match {
      case head::(spare:SPARE)::Nil => spare.addRepairedChild(child, sampleIndex)
      case _ => super.addRepairedChild(child, sampleIndex)
    }
  }
}
