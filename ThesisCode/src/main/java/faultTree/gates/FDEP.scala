package faultTree.gates

import faultTree.model.{LogicalValue, Node}
import org.xml.sax.Attributes

/**
  * Created by pierrj1 on 3/8/2015.
  */
class FDEP(idc: String, attributes: Attributes) extends Gate{
  override val name = "FDEP"
  override val id = idc
   override def function(sampleIndex: Int)(child1: Node, child2: Node) = new LogicalValue(child1.output(sampleIndex) && child2.output(sampleIndex))
  override def output(sampleIndex: Int): Boolean= {
    if(children.head.output(sampleIndex)) children.head.output(sampleIndex)
    else children.tail.reduce(function(sampleIndex)).output(sampleIndex)
  }
 }
