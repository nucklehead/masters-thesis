package Database;
/**
 * Created by pierrj1 on 2/28/2015.
 */

import com.mongodb.*;
import com.mongodb.util.JSON;
import distibutionFunctions.ConstFct;
import distibutionFunctions.ExpFailFct;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import simulation.Element;

import java.awt.geom.Arc2D;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The type Mongo controller.
 */
public class MongoController {
    //todo: look into aerospike or redis

    /**
     * The Elements.
     */
    public DBCollection elements;
    /**
     * The constant db.
     */
    public static DB db;

    static {
        MongoClient mongoClient = null;
        try {
            mongoClient = new MongoClient( "localhost" );
        } catch (UnknownHostException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        db = mongoClient.getDB( "mydb" );
    }

    /**
     * Instantiates a new Mongo controller.
     */
    public MongoController(){
        elements = db.createCollection("sampleElements_"+hashCode(), new BasicDBObject());

        DBCollection origElements = db.getCollection("elements");
        DBCursor cursor = origElements.find();
        try {
            while(cursor.hasNext()) {
                elements.insert(cursor.next());
            }
        } finally {
            cursor.close();
        }
    }

    /**
     * Clear.
     */
    public void clear(){
        elements.drop();
    }

    /**
     * The entry point of application.
     *
     * @param argv the input arguments
     * @throws IOException the io exception
     */
    public static void main(String[] argv) throws IOException {
        String[] names = {"host1", "host2", "switch1", "switch2", "database1", "database2", "storage", "storage backup", "auth provider1", "auth provider2", "terminal1", "terminal2", "spare switch"};


        MongoClient mongoClient = new MongoClient( "localhost" );
        db = mongoClient.getDB( "mydb" );
        if(db.collectionExists("elements"))
            db.getCollection("elements").drop();
        DBCollection tmpElements = db.createCollection("elements", new BasicDBObject());

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        Random rand = new Random();
        for(int i=0; i< 13; i++) {
            Element elem = new Element();
            elem.name = names[i];
            elem.id = "el-" + (i+1);
            elem.backupCost = 20;
            elem.ch = 5;
            elem.co = 7;
            elem.ch = 3;
            elem.cho = 1;
            elem.cwh = 1;
            elem.dataSize = 3;
            elem.dh = .7F;
            elem.dw = .5F;
            elem.failiureFunct = new ExpFailFct(1.0/rand.nextInt(100));
            if(i==12)
                elem.id = "sw-1";

            String json = ow.writeValueAsString(elem);
            DBObject dbObject = (DBObject) JSON.parse(json);
            tmpElements.insert(dbObject);
        }


//        mongoClient.close();
    }

    /**
     * Populate database.
     *
     * @throws IOException the io exception
     */
    public static void populateDatabase() throws IOException {
       /* MongoClient mongoClient = new MongoClient( "localhost" );
        db = mongoClient.getDB( "mydb" );
        DBCollection tmpElements = db.createCollection("elements", new BasicDBObject());

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        for(int i=0; i< 100; i++) {
            String json = ow.writeValueAsString(new Element());
            DBObject dbObject = (DBObject) JSON.parse(json);
            tmpElements.insert(dbObject);
        }


        mongoClient.close();*/

        main(new String[0]);
    }

    /**
     * Get element data size int.
     *
     * @param elemId the elem id
     * @return the int
     */
    public int getElementDataSize(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();
        return Integer.parseInt(element.get("dataSize").toString());
    }

    /**
     * Get element state string.
     *
     * @param elemId the elem id
     * @return the string
     */
    public String getElementState(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();
        return element.get("state").toString();
    }

    /**
     * Set element state.
     *
     * @param elemId the elem id
     * @param state  the state
     */
    public void setElementState(String elemId, String state){
        BasicDBObject query = new BasicDBObject("id", elemId);
        BasicDBObject update = new BasicDBObject("state", state);
        elements.update(query, new BasicDBObject("$set", update));
    }

    /**
     * Set all element state.
     *
     * @param elemId the elem id
     * @param state  the state
     */
    public static void setAllElementState(String elemId, String state){
        BasicDBObject query = new BasicDBObject("id", elemId);
        BasicDBObject update = new BasicDBObject("state", state);
        db.getCollection("elements").update(query, new BasicDBObject("$set", update));
    }

    /**
     * Get element element.
     *
     * @param elemId the elem id
     * @return the element
     */
    public Element getElement(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();
        return new Element(element);
    }

    /**
     * Sets element.
     *
     * @param element the element
     * @throws IOException the io exception
     */
    public void setElement(Element element) throws IOException {
        BasicDBObject query = new BasicDBObject("id", element.id);
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(element);
        DBObject update = (DBObject) JSON.parse(json);
        elements.update(query, new BasicDBObject("$set", update));
    }

    /**
     * Remove element.
     *
     * @param elemId the elem id
     */
    public void removeElement(String elemId){
        elements.findAndRemove(new BasicDBObject("id", elemId));
    }

    /**
     * Get element failiure funct string.
     *
     * @param elemId the elem id
     * @return the string
     */
    public String getElementFailiureFunct(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return element.get("failiureFunct").toString();
    }

    /**
     * Set element original failiure time.
     *
     * @param elemId the elem id
     * @param time   the time
     */
    public void setElementOriginalFailiureTime(String elemId, Float time){
        BasicDBObject query = new BasicDBObject("id", elemId);
        BasicDBObject update = new BasicDBObject("tf_original", time);
        elements.update(query, new BasicDBObject("$set", update));
    }

    /**
     * Set element failiure time.
     *
     * @param elemId the elem id
     * @param time   the time
     */
    public void setElementFailiureTime(String elemId, Float time){
        BasicDBObject query = new BasicDBObject("id", elemId);
        BasicDBObject update = new BasicDBObject("tf", time);
        elements.update(query, new BasicDBObject("$set", update));
    }

    /**
     * Get element original failiure time float.
     *
     * @param elemId the elem id
     * @return the float
     */
    public Float getElementOriginalFailiureTime(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return Float.parseFloat(element.get("tf_original").toString());
    }

    /**
     * Get element failiure time float.
     *
     * @param elemId the elem id
     * @return the float
     */
    public Float getElementFailiureTime(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return Float.parseFloat(element.get("tf").toString());
    }

    /**
     * Get element repair funct string.
     *
     * @param elemId the elem id
     * @return the string
     */
    public String getElementRepairFunct(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return element.get("repairFunct").toString();
    }

    /**
     * Set element repair time.
     *
     * @param elemId the elem id
     * @param time   the time
     */
    public void setElementRepairTime(String elemId, Float time){
        BasicDBObject query = new BasicDBObject("id", elemId);
        BasicDBObject update = new BasicDBObject("tr", time);
        elements.update(query, new BasicDBObject("$set", update));
    }

    /**
     * Get element repair time float.
     *
     * @param elemId the elem id
     * @return the float
     */
    public Float getElementRepairTime(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return Float.parseFloat(element.get("tr").toString());
    }

    /**
     * Get element dw float.
     *
     * @param elemId the elem id
     * @return the float
     */
    public Float getElementDw(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return Float.parseFloat(element.get("dw").toString());
    }

    /**
     * Get element dh float.
     *
     * @param elemId the elem id
     * @return the float
     */
    public Float getElementDh(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return Float.parseFloat(element.get("dh").toString());
    }

    /**
     * Get element cw float.
     *
     * @param elemId the elem id
     * @return the float
     */
    public Float getElementCw(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return Float.parseFloat(element.get("cw").toString());
    }

    /**
     * Get element ch float.
     *
     * @param elemId the elem id
     * @return the float
     */
    public Float getElementCh(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return Float.parseFloat(element.get("ch").toString());
    }

    /**
     * Get element co float.
     *
     * @param elemId the elem id
     * @return the float
     */
    public Float getElementCo(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return Float.parseFloat(element.get("co").toString());
    }

    /**
     * Get backup cost float.
     *
     * @param elemId the elem id
     * @return the float
     */
    public Float getBackupCost(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return Float.parseFloat(element.get("backupCost").toString());
    }

    /**
     * Get time in operation float.
     *
     * @param elemId the elem id
     * @return the float
     */
    public Float getTimeInOperation(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return Float.parseFloat(element.get("timeInOperation").toString());
    }

    /**
     * Add time in operation.
     *
     * @param elemId the elem id
     * @param time   the time
     */
    public void addTimeInOperation(String elemId, Float time){
        BasicDBObject query = new BasicDBObject("id", elemId);
        BasicDBObject update = new BasicDBObject("timeInOperation", time + getTimeInOperation(elemId));
        elements.update(query, new BasicDBObject("$set", update));
    }

    /**
     * Get time in warm float.
     *
     * @param elemId the elem id
     * @return the float
     */
    public Float getTimeInWarm(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return Float.parseFloat(element.get("timeInWarm").toString());
    }

    /**
     * Add time in warm.
     *
     * @param elemId the elem id
     * @param time   the time
     */
    public void addTimeInWarm(String elemId, Float time){
        BasicDBObject query = new BasicDBObject("id", elemId);
        BasicDBObject update = new BasicDBObject("timeInWarm", time + getTimeInWarm(elemId));
        elements.update(query, new BasicDBObject("$set", update));
    }

    /**
     * Get time in hot float.
     *
     * @param elemId the elem id
     * @return the float
     */
    public Float getTimeInHot(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return Float.parseFloat(element.get("timeInHot").toString());
    }

    /**
     * Add time in hot.
     *
     * @param elemId the elem id
     * @param time   the time
     */
    public void addTimeInHot(String elemId, Float time){
        BasicDBObject query = new BasicDBObject("id", elemId);
        BasicDBObject update = new BasicDBObject("timeInHot", time + getTimeInHot(elemId));
        elements.update(query, new BasicDBObject("$set", update));
    }

    /**
     * Get time in failed float.
     *
     * @param elemId the elem id
     * @return the float
     */
    public Float getTimeInFailed(String elemId){
        DBCursor cursor = elements.find(new BasicDBObject("id", elemId));
        DBObject element = cursor.next();
        cursor.close();

        return Float.parseFloat(element.get("timeInFailed").toString());
    }

    /**
     * Add time in failed.
     *
     * @param elemId the elem id
     * @param time   the time
     */
    public void addTimeInFailed(String elemId, Float time){
        BasicDBObject query = new BasicDBObject("id", elemId);
        BasicDBObject update = new BasicDBObject("timeInFailed", time + getTimeInFailed(elemId));
        elements.update(query, new BasicDBObject("$set", update));
    }

    /**
     * Get all elements list.
     *
     * @return the list
     */
    public static List<String> getAllElements(){
        List<String> result = new ArrayList<>();
        DBCursor cursor = db.getCollection("elements").find();

        while(cursor.hasNext()){
            DBObject element = cursor.next();
            result.add(element.get("name").toString() + " ->" + element.get("id").toString());
        }
        cursor.close();

        return result;
    }

    /**
     * Get all element element.
     *
     * @param elemId the elem id
     * @return the element
     */
    public static Element getAllElement(String elemId){
        DBCursor cursor = db.getCollection("elements").find(new BasicDBObject("id", elemId));

        DBObject element = cursor.next();
        cursor.close();

        return new Element(element);
    }

    /**
     * Gets json all element.
     *
     * @param elemId the elem id
     * @return the json all element
     * @throws IOException the io exception
     */
    public static DBObject getJSONAllElement(String elemId) throws IOException {
        DBCursor cursor = db.getCollection("elements").find(new BasicDBObject("id", elemId));
        DBObject element = null;
        if(cursor.hasNext())
            element = cursor.next();
        else {
            ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
            String json = ow.writeValueAsString(new Element());
            element = (DBObject) JSON.parse(json);
            element.put("id", elemId);
        }
        cursor.close();
        return element;
    }

    /**
     * Update json all element.
     *
     * @param elem the elem
     */
    public static void updateJSONAllElement(DBObject elem){
        BasicDBObject query = new BasicDBObject("id", elem.get("id"));
        if(!db.getCollection("elements").update(query, elem).isUpdateOfExisting())
            db.getCollection("elements").insert(elem);
    }

    /**
     * Remove all element.
     *
     * @param elemId the elem id
     */
    public static void removeALLElement(String elemId){
        db.getCollection("elements").findAndRemove(new BasicDBObject("id", elemId));
    }

}
