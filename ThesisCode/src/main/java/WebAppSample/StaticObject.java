package WebAppSample;

import Database.MongoController;
import WebAppSample.shapes.Element;
import javafx.beans.binding.DoubleBinding;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

/**
 * Created by pierrj1 on 8/18/2014.
 */
public /*abstract*/ class StaticObject extends Parent {
    protected final Color FILL_COLOR = Color.BLACK;
    protected final static Font NUMBER_FONT = new Font(16);
    public final Text name = new Text();
    protected Color color = Color.SKYBLUE;
    protected Point2D dragAnchor;
    protected double initX;
    protected double initY;
    protected DoubleBinding containerHeight;
    protected DoubleBinding containerWidth;

    public static double  width = 20;
    public static double height = 40;

    public Element symbol;
    private DraggableObject copy;
    public ListView info;


     StaticObject(final String name, double posx, double posy, DoubleBinding contnWidth, DoubleBinding contnHeight, final Pane parent){
         info = new ListView();
         setTranslateX(posx);
        setTranslateY(posy);
        symbol = new Element();
        getChildren().addAll(symbol);
        configureName(name);
        getChildren().add(this.name);


        containerWidth = contnWidth;
        containerHeight = contnHeight;
        //add a shadow effect
         symbol.shape.setFill(color);
//         symvol.setEffect(new InnerShadow(7, color.darker().darker()));
//         symbol.shape.setStrokeWidth(2);
//         symbol.shape.setStroke(color.darker().darker());
        //change a cursor when it is over object
         symbol.setCursor(Cursor.HAND);
        //add a mouse listeners
        setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                System.out.println("Clicked on" + name + ", " + me.getClickCount() + "times");
                //the event will be passed only to the circle which is on front
                me.consume();
            }
        });
        setOnMouseDragged(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                copy.getOnMouseDragged().handle(me);
            }
        });
        setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
//                toFront();
                symbol.shape.setFill(Color.DARKGRAY);
//                symbol.shape.setStroke(Color.BEIGE.darker().darker().darker());
                System.out.println("Mouse entered " + name);
            }
        });
        setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                symbol.shape.setFill(color);
//                symbol.shape.setStroke(color.darker().darker());
                System.out.println("Mouse exited " +name);
            }
        });
        setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                if (me.isSecondaryButtonDown()) {
                    if (me.isControlDown()) {
                        MongoController.removeALLElement(name.split("->")[1]);
                    }
                    else if (name.contains("->")) {
                        new OptionsParser().updateInfo(info, name);
                        for (Object row : info.getItems()) {
                            if (((HBox) row).getChildren().get(0) instanceof Label)
                                if (((Label) ((HBox) row).getChildren().get(0)).getText().equals("name: ")) {
                                    ((StaticObject) me.getSource()).name.textProperty().bind(((TextField) ((HBox) row).getChildren().get(1)).textProperty().concat(" ->" + name.split("->")[1]));
                                    break;
                                }
                        }
                        DraggableObject.showInfo(info, name, true);
                    }
                }
                else {
                    //when mouse is pressed, store initial position
                    initX = me.getSceneX();
                    initY = me.getSceneY();
                    dragAnchor = new Point2D(me.getSceneX(), me.getSceneY());
                    Element symbolCopy = symbol.clone();

                    copy = new DraggableObject(((StaticObject) me.getSource()).name.getText(), initX, initY, containerWidth, containerHeight, symbolCopy, parent);
                    copy.getOnMousePressed().handle(me);
                }
                System.out.println("Mouse pressed above " + name);
            }
        });
        setOnMouseReleased(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                System.out.println("Mouse released above " +name);
            }
        });

         parent.getChildren().addAll(this);


    }
    StaticObject(final String name, double posx, double posy, DoubleBinding contnWidth, DoubleBinding contnHeight, Element shape, Pane parent){
        this(name, posx, posy, contnWidth, contnHeight, parent);
//        add a shadow effect
        shape.shape.setFill(color);
//         symvol.setEffect(new InnerShadow(7, color.darker().darker()));
//        shape.shape.setStrokeWidth(2);
//        shape.shape.setStroke(color.darker().darker());
        //change a cursor when it is over object
        shape.setCursor(Cursor.HAND);
        symbol = shape;
        getChildren().set(0, shape);
    }
    private void configureName(String string) {
        Font font = Font.font("Tahoma", 9);
        name.setText(string);
        name.setBoundsType(TextBoundsType.VISUAL);
        double width = name.prefWidth(-1);
        double diff = symbol.prefWidth(-1) - width;
        name.setLayoutX(getLayoutX()+diff/4);
        name.setLayoutY(getLayoutY()+15);
        name.setFill(FILL_COLOR);
        name.setFont(font);
    }
}
