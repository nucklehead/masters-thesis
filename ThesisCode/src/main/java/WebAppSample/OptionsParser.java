package WebAppSample;
/**
 * Created by pierrj1 on 3/4/2015.
 */


import Database.MongoController;
import com.mongodb.DBObject;
import com.sun.org.apache.xerces.internal.dom.ElementImpl;
import distibutionFunctions.UserDefineFct;
import faultTree.gates.Gate;
import faultTree.model.Model;
import faultTree.model.Root;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import javafx.util.Callback;
import org.controlsfx.control.PopOver;
import org.controlsfx.control.decoration.Decoration;
import org.controlsfx.validation.ValidationMessage;
import org.controlsfx.validation.ValidationResult;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import simulation.Element;
import simulation.Element2;

import javax.xml.parsers.*;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.rmi.server.UID;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class OptionsParser {
    //to maintain context
    private ListView info;
    public static ValidationSupport validationSupport = new ValidationSupport();
    static {
        ArrayList<PopOver> popOvers = new ArrayList<>();
        validationSupport.validationResultProperty().addListener((observable, oldValue, newValue) -> {
            for(PopOver popOver: popOvers)
                popOver.hide();
            popOvers.clear();
            for(ValidationMessage message: newValue.getErrors()){
                PopOver popOver = new PopOver();
                popOver.setArrowIndent(2);
                popOver.setArrowLocation(PopOver.ArrowLocation.LEFT_TOP);
                popOver.setCornerRadius(10);
                popOver.setContentNode(new Text(message.getText()));
                if(message.getTarget().getScene()!=null) {
                    popOver.show(message.getTarget());
                    popOvers.add(popOver);
                }
            }
        });
    }

    public void updateInfo(ListView info, String name) {
        boolean isNew = info.getItems().isEmpty();
        this.info = info;
        try {
            if(name.contains("->"))
                parseElement(name, isNew);
            else if(name.equals("SPARE"))
                parse(name, getClass().getClassLoader().getResourceAsStream("supported.xml"), isNew);

        }catch(SAXException | ParserConfigurationException | IOException | ClassNotFoundException se) {
            se.printStackTrace();
        }
    }

    public void parse(String name, InputStream file, boolean isNew) throws ParserConfigurationException, IOException, SAXException, ClassNotFoundException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);
        doc.normalizeDocument();

        NodeList gates = doc.getElementsByTagName("gate");
        int k = 0;
        for(int i =0; i< gates.getLength(); i++){
            if(gates.item(i).getAttributes().getNamedItem("type").getNodeValue().equals(name)){
                NodeList properties = gates.item(i).getChildNodes();
                for(int j =0; j< properties.getLength(); j++)
                    if (properties.item(j).getNodeValue() == null) {
                        if (isNew)
                            info.getItems().add(new Property(properties.item(j)));
//                        else
//                            ((Property) info.getItems().get(k++)).set(properties.item(j).getNodeValue());
                    }
            }
        }
    }

    public class Property extends Group{
        public HBox content;
        public Label text;
        public Control options;


        public Property(Node node) throws ClassNotFoundException {

            content = new HBox();
            content.setSpacing(10);
            text = new Label(node.getNodeName());
            content.getChildren().add(text);

            if(node.getAttributes().getNamedItem("type").getNodeValue().equals("ArrayList")) {
                options = new ComboBox<>();
                for(int i =0; i< node.getChildNodes().getLength(); i++){
                    if(node.getChildNodes().item(i).getNodeValue() == null)
                        ((ComboBox)options).getItems().add(node.getChildNodes().item(i).getFirstChild().getNodeValue());
                }
                if(node.getAttributes().getNamedItem("required") != null) {
                    if(node.getAttributes().getNamedItem("required").getNodeValue().equals("true")){
                        validationSupport.registerValidator(options, Validator.createEmptyValidator(text.getText() + " is required"));
                    }
                }
                content.getChildren().add(options);
            }
            else {
                options = new TextField();
                content.getChildren().add(options);
                String type =node.getAttributes().getNamedItem("type").getNodeValue();
                Class <?> clazz = Class.forName(type);
                String finalType = type.split("\\.")[type.split("\\.").length-1];
                Validator<String> validator = (Control c, String newValue) ->
                        ValidationResult.fromErrorIf( options, text.getText() + " should be a " + finalType, isInValid(((TextField) c).getText(), clazz) );
                if(node.getAttributes().getNamedItem("required") != null) {
                    if(node.getAttributes().getNamedItem("required").getNodeValue().equals("true")){
                        validator = Validator.combine(validator, Validator.createEmptyValidator(text.getText() + " is required"));
                    }
                }
                validationSupport.registerValidator(options, validator);
            }
            content.getChildren().get(1).translateXProperty().bind(text.widthProperty().multiply(-1).add(200));
            getChildren().addAll(content);
        }

        public void set(String value){
            if(options instanceof ComboBox)
                ((ComboBox) options).setValue(value);
            else
                ((TextField) options).setText(value);
        }

    }

    public void parseElement(String name, boolean isNew) throws ParserConfigurationException, IOException, SAXException, ClassNotFoundException {
        String elementId = name.split("->")[1];

        DBObject element = MongoController.getJSONAllElement(elementId);
        List<String> keySet = new ArrayList(element.keySet());
        int j =0;
        for(int i =0; i< keySet.size(); i++){
            String key = keySet.get(i);
            HBox content = new HBox();
            content.setSpacing(10);
            Label prop = new Label(key);
            TextField value = new TextField(element.get(key).toString());
            value.textProperty().addListener((observable, oldValue, newValue) -> {
                element.put(key, newValue);
            });
            if(!key.matches("(t[ohrfw].*|_id|timeIn.*|state)")) {
                if(!isNew){
                    content = (HBox) info.getItems().get(j++);
                    TextField currentValue = (TextField) content.getChildren().get(1);
                    currentValue.setText(element.get(key).toString());
                    continue;
                }
                Validator<String> validator = Validator.createEmptyValidator(key + " is required");
                switch (key){
                    case "id":
                        value.setDisable(true);
                        break;
                    case "name":
                        break;
                    case "dataSize":
                        validator = Validator.combine(validator, (Control c, String newValue) ->
                            ValidationResult.fromErrorIf( value, "Incorrect " + key + " format", isInValid(value.getText(), Integer.class)));
                        break;
                    case "repairFunct":
                    case "failiureFunct":
                        validator = Validator.combine(validator, (Control c, String newValue) ->
                                ValidationResult.fromErrorIf( value, "Incorrect " + key + " format", isInValid(value.getText(), UserDefineFct.class)));
                        break;
                    default:
                        validator = Validator.combine(validator, (Control c, String newValue) ->
                                ValidationResult.fromErrorIf( value, "Incorrect " + key + " format", isInValid(value.getText(), Float.class)));
                }
                validationSupport.registerValidator(value, validator);
                content.getChildren().addAll(prop, value);
                info.getItems().add(content);
            }
        }
        if(isNew) {
            Button saveButton = new Button("Update Database");
            saveButton.setPrefWidth(200);
            saveButton.setPrefHeight(25);
            saveButton.setOnAction(event -> {
                MongoController.updateJSONAllElement(element);
            });
            HBox.setMargin(saveButton, new Insets(10,0,10,100));
            info.getItems().add(new HBox(saveButton));
        }
    }

    static boolean isInValid(String value, Class<?> clazz){
        if(value == null)
            return false;
        if(value.isEmpty())
            return false;
        try {
            Constructor<?> cons = clazz.getConstructor(String.class);
            cons.newInstance(value);
            return false;
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            return true;
        }

    }
}