package WebAppSample;

/**
 * Created by pierrj1 on 8/28/2014.
 */

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SymbolCloner
{
    private SymbolCloner(){}
    @SuppressWarnings("unchecked")
    public static  <T>  T cloneSymbol(T t) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Class<T> type = (Class<T>) t.getClass();
        Constructor<T> consts = type.getDeclaredConstructor();
    return consts.newInstance();
    }

}