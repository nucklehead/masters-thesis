package WebAppSample;

import javafx.application.Platform;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * TODO This Class should be replaced by some Workmanager implemntation
 * in the future
 * @author Heist
 */
public class FxPlatformExecutor {
    
    public static void runOnFxApplication(Runnable task) {
        if (Platform.isFxApplicationThread()) {
            task.run();
        } else {
            Platform.runLater(task);
        }
    }
    public static void runOnFxApplicationAndWait(final Runnable run) {
        if (Platform.isFxApplicationThread()) {
          run.run();
        } else {
            final Lock lock = new ReentrantLock();
            final Condition condition = lock.newCondition();
            lock.lock();
            try {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        lock.lock();
                        try {
                            run.run();
                        } finally {
                            try {
                                condition.signal();
                            } finally {
                                lock.unlock();
                            }
                        }
                    }
                });
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }
}
