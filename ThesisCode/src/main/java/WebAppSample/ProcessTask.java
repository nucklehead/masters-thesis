package WebAppSample;

import javafx.concurrent.Task;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import simulation.MCSimulator;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by pierrj1 on 5/25/2015.
 */
public abstract class ProcessTask extends Task<Void>{

    protected ProcessTask() {
        setOnFailed(event -> {
            update(0,0);
            StringWriter stringWriter = new StringWriter();
            PrintWriter writer = new PrintWriter(stringWriter);
            event.getSource().getException().printStackTrace(writer);
            Notifications notifications = Notifications.create()
                    .title("Simulation failed.")
                    .text(stringWriter.toString())
                    .hideAfter(Duration.INDEFINITE)
                    .owner(HomePage.taskProgressView.getScene().getWindow());
            notifications.showConfirm();
        });
    }

    public void update(long workDone, long max){
        updateProgress(workDone, max);
     }

};

