package WebAppSample.shapes;

import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;

/**
 * Created by pierrj1 on 5/3/2015.
 */
public class PAND extends Gate {

    public PAND() {
        Rectangle bottom = new Rectangle(base.getX(), base.getY()+20, 40, 20);
        Arc top = new Arc(bottom.getX()+20, bottom.getY(), 20, 20, 0, 180);
        Polygon polygon = new Polygon(bottom.getX(), bottom.getY()+18, bottom.getX()+20, bottom.getY()-20, bottom.getX()+40, bottom.getY()+18);
        Polygon polygonIn = new Polygon(bottom.getX()+5, bottom.getY()+15, bottom.getX()+20, bottom.getY()-15, bottom.getX()+35, bottom.getY()+15);
        shape = Path.union(Path.subtract(Path.union(top, bottom), polygon), polygonIn);
        shape.setFill(Color.SKYBLUE);
        getChildren().addAll(shape);
    }
}
