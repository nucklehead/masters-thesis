package WebAppSample.shapes;

import WebAppSample.DraggableObject;
import WebAppSample.OptionsParser;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by pierrj1 on 5/3/2015.
 */
public class Element extends Parent{
    public Pin pinIn;
    public Pin pinOut;

    public Shape shape;

    public void setChildren(ArrayList<javafx.scene.Node> children){
        getChildren().clear();
        getChildren().addAll(children);
    }
    public Element() {
        shape = new Circle(20, 20, 20);
        pinOut = new Pin("out", 20, 0, 20, -10);

        shape.setFill(Color.SKYBLUE);
        getChildren().addAll(pinOut, shape);
    }

    public void removePins(){
        List<javafx.scene.Node> nodes = new ArrayList<>();
        nodes.addAll(getChildren());
        for (javafx.scene.Node pin: nodes){
            if(pin instanceof Pin)
                getChildren().remove(pin);
        }
    }

    public Element clone(){
        try {
            return this.getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            System.exit(-2);
        }
        return null;
    }
    public Node toxmL(Document xml, Node node){

        String nameID[] = ((DraggableObject)getParent()).name.getText().split("->");
        String name = nameID[0];
        org.w3c.dom.Element element = xml.createElement("gate");

        if(this instanceof Root)
            element = xml.createElement("root");

        if(this.getClass().isAssignableFrom(Element.class)) {
            element = xml.createElement("element");
            String id = nameID[1];
            element.setAttribute("id", id);
        }

        element.setAttribute("name", name);
        element.setAttribute("type", this.getClass().getSimpleName());

        element.setAttribute("translatex", String.valueOf(getParent().getTranslateX()));
        element.setAttribute("translatey", String.valueOf(getParent().getTranslateY()));


        ListView info = ((DraggableObject)getParent()).info;
        if(info.getItems().size()> 0){
            if( info.getItems().get(0) instanceof OptionsParser.Property ){
                for(Object prop : info.getItems()){
                    OptionsParser.Property property = (OptionsParser.Property) prop;
                    if (property.options instanceof ComboBox) {
                        if (!((ComboBox) property.options).getSelectionModel().isEmpty())
                            element.setAttribute(property.text.getText(), ((ComboBox) property.options).getValue().toString());
                    }
                    else
                    if(((TextField) property.options).getText() != null)
                        if(!((TextField) property.options).getText().isEmpty())
                            element.setAttribute(property.text.getText(), ((TextField) property.options).getText());
                }
            }
        }

        node.appendChild(element);
        return  element;
    }

    public boolean hasEmptyLeftPin(){
        return getChildren().size() == 4 && pinIn.to.size() == 0;
    }
}
