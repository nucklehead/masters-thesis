package WebAppSample.shapes;

import WebAppSample.DraggableObject;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by pierrj1 on 5/9/2015.
 */
public abstract class Gate extends Element {
    protected Rectangle base;

    Gate(){
        base = new Rectangle(40,40);
        base.setFill(Color.TRANSPARENT);
        getChildren().add(base);
        pinOut = new Pin("out", base.getX()+20, base.getY(), base.getX()+20, base.getY()-10);
        pinIn = new Pin("in", base.getX()+20, base.getY()+40, base.getX()+20, base.getY()+50);

        getChildren().clear();
        getChildren().addAll(pinIn, pinOut);
    }

    @Override
    public org.w3c.dom.Node toxmL(Document xml, org.w3c.dom.Node node){
        org.w3c.dom.Node element = super.toxmL(xml, node);
        for(Pin pin : pinIn.to){
            ((DraggableObject)pin.getParent().getParent()).symbol.toxmL(xml, element);
        }
        return element;
    }
}
