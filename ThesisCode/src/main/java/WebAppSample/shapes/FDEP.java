package WebAppSample.shapes;

import javafx.scene.paint.Color;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;

/**
 * Created by pierrj1 on 5/3/2015.
 */
public class FDEP extends SidePinGate {
    public FDEP() {
        super("left");
        Rectangle bottom = new Rectangle(base.getX(), base.getY()+20, 40, 17);
        Rectangle top = new Rectangle(bottom.getX(), bottom.getY()-23, 40, 22);
        double lenth = Math.sqrt(Math.pow(bottom.getHeight()/2, 2) + Math.pow(bottom.getHeight(), 2));
        Polygon trigger = new Polygon(bottom.getX()+2, bottom.getY(), bottom.getX()+2, bottom.getY()+17, lenth, bottom.getY()+17.0/2);
        shape = Path.subtract(Path.union(top, bottom), trigger);
        shape.setFill(Color.SKYBLUE);
        pinIn.to = pinSide.to;
        getChildren().addAll( shape);
    }
}
