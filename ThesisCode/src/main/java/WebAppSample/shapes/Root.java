package WebAppSample.shapes;

import WebAppSample.DraggableObject;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import org.codehaus.jackson.map.util.JSONPObject;
import org.json.JSONArray;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.json.JSONObject;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;


/**
 * Created by pierrj1 on 5/9/2015.
 */
public class Root extends Element {
    protected Rectangle base;

    public Root(){
        base = new Rectangle(80,40);
        base.setFill(Color.SKYBLUE);
        pinIn = new Pin("in", base.getX()+40, base.getY()+40, base.getX()+40, base.getY()+50);

        getChildren().clear();
        getChildren().addAll(pinIn, base);
    }

    public void removePins(){
        getChildren().removeAll(pinIn, pinOut);
    }

    public void createXml(String fileLocation) throws ParserConfigurationException, FileNotFoundException, TransformerException {

        if(fileLocation == null)
            fileLocation = getClass().getClassLoader().getResource("").getPath() + "tmpModel.ftmod";

        PrintWriter outfile = new PrintWriter (fileLocation);
        Document xml = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

        org.w3c.dom.Element element = xml.createElement("model");
        element.setAttribute("name", "App test Model");
        xml.appendChild(element);

        DOMSource domSource = new DOMSource(toxmL(xml, element));
        StreamResult result = new StreamResult(outfile);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.transform(domSource, result);

        outfile.close();

    }
    @Override
    public Node toxmL(Document xml, Node node){
        Node element = super.toxmL(xml, node);
        for(Pin pin : pinIn.to){
            ((DraggableObject)pin.getParent().getParent()).symbol.toxmL(xml, element);
        }
        return xml;
    }
}
