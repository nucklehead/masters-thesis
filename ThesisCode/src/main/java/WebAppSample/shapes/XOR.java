package WebAppSample.shapes;

import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;

/**
 * Created by pierrj1 on 5/3/2015.
 */
public class XOR extends Gate {
    public XOR() {
        Rectangle bottom = new Rectangle(base.getX(), base.getY()+35, 40, 5);
        Arc top1 = new Arc(bottom.getX(), bottom.getY(), 40, 40, 0, 180);
        Arc top2 = new Arc(bottom.getX()+40, bottom.getY(), 40, 40, 0, 180);
        Circle sub = new Circle(bottom.getX()+20, bottom.getY()+27, 30);
        Polygon polygon = new Polygon(bottom.getX()-5, bottom.getY()+20, bottom.getX()+20, bottom.getY()-35, bottom.getX()+45, bottom.getY()+20);
        Polygon polygonIn = new Polygon(bottom.getX(), bottom.getY()+15, bottom.getX()+20, bottom.getY()-30, bottom.getX()+40, bottom.getY()+15);

        shape = Path.subtract(Path.union(Path.subtract(Path.union(Path.intersect(top1, top2), bottom), polygon), polygonIn), sub);
        shape.setFill(Color.SKYBLUE);
        getChildren().addAll( shape);
    }
}
