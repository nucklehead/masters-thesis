package WebAppSample.shapes;

/**
 * Created by pierrj1 on 5/3/2015.
 */
public abstract class SidePinGate extends Gate {
    public Pin pinSide;
    public SidePinGate(String side) {
        if(side.equals("left"))
            pinSide = new Pin("in-left", base.getX(), base.getY()+20+17.0/2, base.getX()-10, base.getY()+20+17.0/2);
        else if(side.equals("right"))
            pinSide = new Pin("in-right", 40, 20, 50, 20);
        getChildren().addAll(pinSide);
    }
}
