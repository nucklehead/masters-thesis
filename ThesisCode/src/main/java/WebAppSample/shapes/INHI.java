package WebAppSample.shapes;

import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;

/**
 * Created by pierrj1 on 5/3/2015.
 */
public class INHI extends SidePinGate {
    public INHI() {
        super("right");
        double height = 20/Math.sqrt(3);
        shape = new Polygon(0, height, 0, 40-height, 20, 40, 40, 40-height, 40, height, 20, 0);
        shape.setFill(Color.SKYBLUE);
        pinIn.to = pinSide.to;
        getChildren().addAll(shape);
    }
}
