package WebAppSample.shapes;

import javafx.scene.paint.Color;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;

/**
 * Created by pierrj1 on 5/3/2015.
 */
public class SPARE extends SidePinGate {
    public SPARE() {
        super("left");
        Rectangle bottom = new Rectangle(base.getX(), base.getY()+20, 40, 17);
        Rectangle top = new Rectangle(bottom.getX(), bottom.getY()-23, 40, 22);
        Rectangle spares = new Rectangle(bottom.getX()+19, bottom.getY()+2.5, 20, 12);
        shape = Path.subtract(Path.union(top, bottom), spares);
        shape.setFill(Color.SKYBLUE);
        pinIn.to = pinSide.to;
        getChildren().addAll(shape);
    }
}
