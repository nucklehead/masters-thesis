package WebAppSample.shapes;

import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

/**
 * Created by pierrj1 on 5/3/2015.
 */
public class AND extends Gate {
    public AND() {
        Rectangle bottom = new Rectangle(base.getX(), base.getY()+20, 40, 20);
        Arc top = new Arc(bottom.getX()+20, bottom.getY(), 20, 20, 0, 180);
        shape = Path.union(top, bottom);
        shape.setFill(Color.SKYBLUE);
        getChildren().addAll(shape);
    }
}
