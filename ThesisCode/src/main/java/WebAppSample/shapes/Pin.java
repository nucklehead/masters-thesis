package WebAppSample.shapes;

import javafx.beans.binding.DoubleBinding;
import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;


/**
 * Created by pierrj1 on 5/9/2015.
 */
public class Pin extends Parent {

    public static Semaphore waitForInit = new Semaphore(0);

    public static Pin clicked = null;
    public Line line;
    public String name;

    public List<Pin> to = new ArrayList<>();
    public Pin parent;

    public Pin() {
        setOnMouseEntered(event -> {
            if (event.isControlDown() && name.equals("out"))
                line.setStroke(Color.DARKRED);

        });
        setOnMouseExited(event -> {
            if (event.isControlDown() && name.equals("out"))
                line.setStroke(Color.SKYBLUE.darker().darker());

        });
        setOnMouseClicked(event -> {
            if (event.isControlDown() && parent != null) {
                parent.to.remove(event.getSource());
                getChildren().remove(getChildren().size() - 3, getChildren().size());
                parent = null;
                line.setStroke(Color.SKYBLUE.darker().darker());
            } else if (clicked == null && name.equals("out")) {
                line.setStroke(Color.OLIVE.brighter());
                line.setStrokeWidth(6);
                clicked = this;
            } else if (clicked != null) {
                if (name.contains("in") && !to.contains(clicked)) {

                    DoubleBinding shiftX = getParent().getParent().translateXProperty().subtract(clicked.getParent().getParent().translateXProperty());
                    DoubleBinding shiftY = getParent().getParent().translateYProperty().subtract(clicked.getParent().getParent().translateYProperty());

                    Line ver1 = new Line();
                    ver1.startXProperty().bind(line.endXProperty().add(shiftX));
                    ver1.startYProperty().bind(line.endYProperty().add(shiftY));

                    ver1.endXProperty().bind(line.endXProperty().add(shiftX));
                    ver1.endYProperty().bind(clicked.line.endYProperty().add(shiftY).divide(2));
                    ver1.setStroke(Color.SKYBLUE.darker().darker());
                    ver1.setStrokeWidth(4);

                    Line hor = new Line();
                    hor.startXProperty().bind(ver1.endXProperty());
                    hor.startYProperty().bind(ver1.endYProperty());

                    hor.endXProperty().bind(clicked.line.endXProperty());
                    hor.endYProperty().bind(ver1.endYProperty());
                    hor.setStroke(Color.SKYBLUE.darker().darker());
                    hor.setStrokeWidth(4);

                    Line ver2 = new Line();
                    ver2.startXProperty().bind(hor.endXProperty());
                    ver2.startYProperty().bind(hor.endYProperty());

                    ver2.endXProperty().bind(clicked.line.endXProperty());
                    ver2.endYProperty().bind(clicked.line.endYProperty());
                    ver2.setStroke(Color.SKYBLUE.darker().darker());
                    ver2.setStrokeWidth(4);

                    clicked.getChildren().addAll(ver1, hor, ver2);

                    clicked.line.setStroke(Color.SKYBLUE.darker().darker());
                    clicked.line.setStrokeWidth(4);
                    if(name.matches("(.*-left|.*-right)"))
                        to.add(0,clicked);
                    else
                        to.add(clicked);
                    clicked.parent = this;
                    clicked = null;
                }
            }
            waitForInit.release();
        });
    }

    public Pin(String name, double startX, double startY, double endX, double endY) {
        this();
        line = new Line(startX, startY, endX, endY);
        this.name = name;
        line.setStroke(Color.SKYBLUE.darker().darker());
        line.setStrokeWidth(4);
        getChildren().add(line);
    }


}
