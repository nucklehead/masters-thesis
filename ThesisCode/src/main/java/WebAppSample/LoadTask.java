package WebAppSample;

import faultTree.model.SAXModelParser;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.Pane;

import java.io.File;

/**
 * Created by pierrj1 on 5/25/2015.
 */
public class LoadTask extends ProcessTask{
    Pane root;
    File file;

    public LoadTask(Pane root, File file) {
        this.root = root;
        this.file = file;
    }

    @Override
    protected Void call() throws Exception {
        SAXModelParser parser = new SAXModelParser(file.getPath());
        updateProgress(1, 3);
        parser.appView = root;
        new Thread(parser::parseDocument).run();
        return null;
    }
}

