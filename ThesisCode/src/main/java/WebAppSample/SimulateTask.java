package WebAppSample;

import faultTree.model.SAXModelParser;
import javafx.concurrent.Task;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import simulation.MCSimulator;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by pierrj1 on 5/25/2015.
 */
public class SimulateTask extends ProcessTask{
    String misstionTime;
    String threshold;
    String concurrentThread;
    String modelFile;

    public SimulateTask(String misstionTime, String threshold, String concurrentThread, String modelFile) {
        this.misstionTime = misstionTime;
        this.threshold = threshold;
        this.concurrentThread = concurrentThread;
        this.modelFile = modelFile;
        setOnSucceeded(event -> {
            Notifications notifications = Notifications.create()
                    .title("Simulation completed. \nResults are:")
                    .text(MCSimulator.results().toString())
                    .hideAfter(Duration.INDEFINITE)
                    .owner(HomePage.taskProgressView.getScene().getWindow());
            notifications.showConfirm();
        });
    }
    @Override
    protected Void call() throws Exception {
        MCSimulator.initialize(misstionTime, threshold, concurrentThread, modelFile);
        new Thread(MCSimulator::run).run();
        return null;
    }
};

