package WebAppSample;

import WebAppSample.shapes.Element;
import WebAppSample.shapes.Root;
import javafx.beans.binding.DoubleBinding;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.control.Notifications;
import org.controlsfx.validation.ValidationMessage;

/**
 * Created by pierrj1 on 8/18/2014.
 */
public /*abstract*/ class DraggableObject extends StaticObject {
    DraggableObject(final String name, double posx, double posy, DoubleBinding contnWidth, DoubleBinding contnHeight, final Pane parent){
        super(name, posx, posy, contnWidth, contnHeight, parent);
        setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                if (me.getClickCount() == 2 && (name.equals("SPARE") || name.contains("->"))) {
                    new OptionsParser().updateInfo(info, name);
                    if(name.contains("->"))
                        for(Object row : info.getItems()){
                            if(((HBox)row).getChildren().get(0) instanceof Label)
                                if(((Label)((HBox)row).getChildren().get(0)).getText().equals("name: ")) {
                                    ((DraggableObject)me.getSource()).name.textProperty().bind(((TextField)((HBox)row).getChildren().get(1)).textProperty().concat(" ->" + name.split("->")[1]));
                                    break;
                                }
                        }
                    showInfo(info, name, true);
                }
                if (me.getButton().equals(MouseButton.SECONDARY) && !(symbol instanceof Root)) {
                    parent.getChildren().remove(me.getSource());
                }

            }
        });
        setOnMousePressed(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                //when mouse is pressed, store initial position
                initX = getTranslateX();
                initY = getTranslateY();
                dragAnchor = new Point2D(me.getSceneX(), me.getSceneY());
                System.out.println("Mouse pressed above " + name);
            }
        });
        setOnMouseDragged(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                double dragX = me.getSceneX() - dragAnchor.getX();
                double dragY = me.getSceneY() - dragAnchor.getY();
                //calculate new position of the object
                double newXPosition = initX + dragX;
                double newYPosition = initY + dragY;
                //if new position do not exceeds borders of the container, translate to this position
                if ((newXPosition>=-containerWidth.doubleValue()) && (newXPosition<=containerWidth.doubleValue())) {
                    setTranslateX(newXPosition);
                }
                if ((newYPosition>=-containerHeight.doubleValue()) && (newYPosition<=containerHeight.doubleValue())){
                    setTranslateY(newYPosition);
                }
                System.out.println(name + " was dragged (x:" + dragX + ", y:" + dragY +")");
            }
        });

    }
    public DraggableObject(final String name, double posx, double posy, DoubleBinding contnWidth, DoubleBinding contnHeight, Element shape, Pane parent){
        this(name, posx, posy, contnWidth, contnHeight, parent);
//        add a shadow effect
        shape.shape.setFill(color);
//         symvol.setEffect(new InnerShadow(7, color.darker().darker()));
//        shape.shape.setStrokeWidth(2);
//        shape.shape.setStroke(color.darker().darker());
        //change a cursor when it is over object
        shape.setCursor(Cursor.HAND);
        symbol = shape;
        getChildren().set(0, shape);
    }

    public static void showInfo(ListView info, String name, Boolean veriFyInput){
        info.setVisible(true);
        Group pane = new Group(info);
        Scene properties = new Scene(pane, 400, info.getItems().size()*500/15.0);
        properties.getStylesheets().add(DraggableObject.class.getClassLoader().getResource("validation.css").toExternalForm());
        info.prefHeightProperty().bind(properties.heightProperty());
        info.prefWidthProperty().bind(properties.widthProperty());
        Stage popup = new Stage();
        popup.initModality(Modality.APPLICATION_MODAL);
        popup.setScene(properties);
        popup.show();
        System.out.println("Mouse clicked above " + name);
        popup.setOnCloseRequest(event -> {
            if (name.matches("(SPARE|.*->el-.*)") && veriFyInput) {
                if (OptionsParser.validationSupport.isInvalid()) {
                    String errors = "";
                    for (ValidationMessage message : OptionsParser.validationSupport.getValidationResult().getErrors())
                        errors += message + "\n";
                    Notifications notifications = Notifications.create()
                            .title("Some properties are invalid.")
                            .text(errors)
                            .owner(properties.getWindow());
                    notifications.showError();
                    event.consume();
                }
            }
        });
    }
}
