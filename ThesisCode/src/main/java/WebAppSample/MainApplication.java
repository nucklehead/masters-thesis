package WebAppSample;

import WebAppSample.shapes.*;
import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.Reflection;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by pierrj1 on 8/18/2014.
 */
public class MainApplication extends Application{

    String titleText = "MCS Reliability Framework";
    Text title;
    List<Gate> bottom;


    private void init(final Stage primaryStage) {


        title = new Text();
        title.setFill(Color.web("#0099CC"));
        title.setFont(Font.font(null, FontWeight.BOLD, 40));
        Reflection r = new Reflection();
        r.setFraction(0.7f);
        title.setEffect(r);

        HBox gates = new HBox();
        gates.setSpacing(20);
        gates.setPadding(new Insets(20));

        bottom = new ArrayList<>();
        bottom.addAll(Arrays.asList(new AND(), new OR(), new PAND(), new XOR(), new INHI(), new SPARE(), new FDEP()));
        for (Gate gate: bottom)
            gate.removePins();
        gates.getChildren().addAll(bottom);
        gates.setAlignment(Pos.BASELINE_CENTER);
        final VBox root = new VBox();
        root.getChildren().addAll(title, gates);

        root.setAlignment(Pos.CENTER);
        root.setSpacing(30);
        primaryStage.setResizable(true);
        primaryStage.setScene(new Scene(root, 1200, 800));

        animation.setOnFinished(event -> {
            try {
                new HomePage(primaryStage.getScene());
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(-1);
            }

        });

        primaryStage.setOnShown(event -> {
            animation.play();
            gateAnimation.play();
        });

    }

    final Animation animation = new Transition() {
        {
            setCycleDuration(Duration.millis(2000));
        }

        protected void interpolate(double frac) {
            final int length = titleText.length();
            final int n = Math.round(length * (float) frac);
            title.setText(titleText.substring(0, n));
        }

    };

    final Animation gateAnimation = new Transition() {
        {
            setCycleDuration(Duration.millis(2000));
        }

        protected void interpolate(double frac) {
            for(int i=0; i < bottom.size(); i++){
                if(i%2 ==0)
                    bottom.get(i).setTranslateY( 20 * Math.sin(2 * Math.PI * frac ) );
                else
                    bottom.get(i).setTranslateY( -20 * Math.sin(2 * Math.PI * frac ) );
            }
        }
    };

    public void showApp(){

    }

    @Override
    public void stop() throws Exception {
        HomePage.executorService.shutdownNow();
        super.stop();
    }

    @Override public void start(Stage primaryStage) throws Exception {
        init(primaryStage);
        primaryStage.show();
    }
    public static void main(String[] args) { launch(args); }

}
