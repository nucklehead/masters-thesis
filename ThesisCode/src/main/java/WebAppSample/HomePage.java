package WebAppSample;

import Database.MongoController;
import WebAppSample.shapes.*;
import faultTree.model.SAXModelParser;
import javafx.beans.NamedArg;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Popup;
import org.controlsfx.control.Notifications;
import org.controlsfx.control.TaskProgressView;
import org.controlsfx.validation.ValidationMessage;
import simulation.MCSimulator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by pierrj1 on 5/9/2015.
 */
public class HomePage {
    public static TaskProgressView<Task<Void>> taskProgressView = new TaskProgressView<>();
    public static VBox sidePane;
    public static ExecutorService executorService =  Executors.newCachedThreadPool();
    public static Root xmlRoot;
    List<Node> home;
    public HomePage( Scene  scene) throws IOException {
//        scene.add(MouseEvent.MOUSE_CLICKED, )

        Pane root = FXMLLoader.load(getClass().getClassLoader().getResource("HomePage.fxml"));

        home = new ArrayList<>(root.getChildren());
        sidePane = (VBox) root.getChildren().get(0);

        sidePane.layoutXProperty().bind(scene.widthProperty().subtract(sidePane.prefWidthProperty()));
        sidePane.prefWidthProperty().bind(scene.widthProperty().divide(4));

        Separator seperator = (Separator) root.getChildren().get(root.getChildren().size()-1);
        seperator.layoutXProperty().bind(scene.widthProperty().subtract(sidePane.prefWidthProperty()));
        seperator.prefHeightProperty().bind(scene.heightProperty());
        sidePane.prefHeightProperty().bind(scene.heightProperty());




        TilePane faultTreeItems = (TilePane) sidePane.getChildren().get(0);
        List<Gate> gates = Arrays.asList(new AND(), new OR(), new PAND(), new XOR(), new INHI(), new SPARE(), new FDEP());
        for (Gate gate: gates)
            faultTreeItems.getChildren().add(new StaticObject(gate.getClass().getSimpleName(), 0, 0, sidePane.layoutXProperty().subtract(40), root.heightProperty().add(0), gate, root ));

        faultTreeItems.setHgap(5);
        faultTreeItems.setVgap(5);
        xmlRoot = new Root();
        new DraggableObject("System Failure", 100, 10, sidePane.layoutXProperty().subtract(40), root.heightProperty().add(0), xmlRoot, root);
        ScrollPane scrollPane = ((ScrollPane)sidePane.getChildren().get(sidePane.getChildren().size()-1));
        TilePane elementsItems = (TilePane) scrollPane.getContent();
        elementsItems.prefColumnsProperty().bind(sidePane.widthProperty().divide(87.5));
        List<String> elementsId = MongoController.getAllElements();
        for(String id: elementsId)
            elementsItems.getChildren().add(new StaticObject(id, 0, 0, sidePane.layoutXProperty().subtract(40), root.heightProperty().add(0), new Element(), root ));

        StaticObject createElement = new StaticObject("New Element", 0, 0, sidePane.layoutXProperty().subtract(40), root.heightProperty().add(0), new Element(), root );
        createElement.color = Color.CORAL;
        createElement.symbol.shape.setFill(Color.CORAL);
        createElement.symbol.removePins();
        createElement.addEventFilter(EventType.ROOT, event -> {
            if(!(event.getEventType().equals(MouseEvent.MOUSE_CLICKED) || event.getEventType().equals(MouseEvent.MOUSE_ENTERED) || event.getEventType().equals(MouseEvent.MOUSE_EXITED)))
                event.consume();
        });
        createElement.setOnMousePressed(event -> {});
        createElement.setOnMouseClicked(event -> {
            int idNum = elementsItems.getChildren().size();
            String name = " ->el-" + idNum;
            StaticObject newElement = new StaticObject(name, 0, 0, sidePane.layoutXProperty().subtract(40), root.heightProperty().add(0), new Element(), root );
            newElement.fireEvent(new MouseEvent(MouseEvent.MOUSE_PRESSED, 0, 0, 0, 0,MouseButton.SECONDARY, 1, false, false, false, false, false, false, true, false, false, false, null));
            elementsItems.getChildren().add(newElement);
        });
        elementsItems.getChildren().add(0, createElement);
        elementsItems.setHgap(5);
        elementsItems.setVgap(5);

        HBox buttons = (HBox) sidePane.getChildren().get(2);
        Button saveButtuon = (Button) buttons.getChildren().get(0);
        saveButtuon.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save Model");
            fileChooser.setInitialDirectory(
                    new File(System.getProperty("user.home"))
            );
            fileChooser.getExtensionFilters().add(
                    new FileChooser.ExtensionFilter("Fault Tree Model", "*.ftmod")
            );
            File file = fileChooser.showSaveDialog(scene.getWindow());
            if (file != null) {
                try {
                    xmlRoot.createXml(file.getPath());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Button simulateButton = (Button) buttons.getChildren().get(1);
        GridPane parametters = (GridPane) sidePane.getChildren().get(3);
        simulateButton.setOnAction(event -> {
            try {
                if (OptionsParser.validationSupport.isInvalid()) {
                    String errors = "";
                    for(ValidationMessage message: OptionsParser.validationSupport.getValidationResult().getErrors())
                        errors += message + "\n";
                    Notifications notifications = Notifications.create()
                            .title("Some components of the model have invalid configurations.")
                            .text(errors)
                            .owner(scene.getWindow());
                    notifications.showError();
                }
                else {
                    xmlRoot.createXml(null);
                    String file =  getClass().getClassLoader().getResource("").getPath() + "tmpModel.ftmod";
                    SimulateTask task = new SimulateTask(
                            ((TextField) parametters.getChildren().get(1)).getText(),
                            ((TextField) parametters.getChildren().get(3)).getText(),
                            ((TextField) parametters.getChildren().get(5)).getText(),
                            file);
                    taskProgressView.getTasks().add(task);
                    executorService.submit(task);
                }

            } catch (ParserConfigurationException | FileNotFoundException | TransformerException e) {
                Popup popup= new Popup();
                Label label = new Label(e.getMessage());
                label.setTextFill(Color.RED);
                popup.getContent().add(new HBox(label, new ListView<>(FXCollections.observableArrayList(e.getStackTrace()))));
                popup.show(scene.getWindow());
            }

        });

        Button loadButtuon = (Button) buttons.getChildren().get(2);
        loadButtuon.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open model");
            fileChooser.setInitialDirectory(
                    new File(System.getProperty("user.home"))
            );
            fileChooser.getExtensionFilters().add(
                    new FileChooser.ExtensionFilter("Fault Tree Model", "*.ftmod")
            );
            File file = fileChooser.showOpenDialog(scene.getWindow());
            if (file != null) {
                root.getChildren().clear();
                root.getChildren().addAll(home);
                LoadTask task = new LoadTask(root, file);
                taskProgressView.getTasks().add(task);
                executorService.submit(task);
            }
        });
        taskProgressView.setMaxHeight(50);
        taskProgressView.minHeight(50);
//        VBox.setVgrow(taskProgressView, Priority.NEVER);
        sidePane.getChildren().add(taskProgressView);
        scene.setRoot(root);
    }


}
